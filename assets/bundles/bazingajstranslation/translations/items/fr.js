(function (t) {
// fr
t.add("Armure l\u00e9g\u00e8re", "Armure l\u00e9g\u00e8re", "items", "fr");
t.add("V\u00eatements robustes", "V\u00eatements robustes", "items", "fr");
t.add("V\u00eatements traditionnels", "V\u00eatements traditionnels", "items", "fr");
t.add("V\u00eatements  traditionnels", "V\u00eatements traditionnels", "items", "fr");
t.add("N\u00e9cessaire de Calligraphie", "N\u00e9cessaire de Calligraphie", "items", "fr");
t.add("N\u00e9cessaire  de  Calligraphie", "N\u00e9cessaire de Calligraphie", "items", "fr");
t.add("N\u00e9cessaire de Voyage", "N\u00e9cessaire de Voyage", "items", "fr");
t.add("Couteau", "Couteau", "items", "fr");
})(Translator);
