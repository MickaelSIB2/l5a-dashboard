
var $ = require('jquery');

import 'jquery-ui-bundle';
require('bootstrap');
require('bootstrap/dist/css/bootstrap.css');

require('../css/app.css');
// require('../css/colors.css');
require('../css/form.css');
require('jquery-ui-bundle/jquery-ui.css');
require('jquery-ui-bundle/jquery-ui.structure.css');
require('jquery-ui-bundle/jquery-ui.theme.css');
//require('../css/responsive.css');
require('../css/spin.css');
require('../css/utilities.css');

//require('./functions');
require('./plugins/popup.js');

window.Translator = require('bazinga-translator/js/translator');
require( '../bundles/bazingajstranslation/translations/config.js');
require('../bundles/bazingajstranslation/translations/messages/fr.js'); // js code only uses message domain anyway
require('../bundles/bazingajstranslation/translations/messages/en.js');

window.toastr = require('toastr');
require('toastr/build/toastr.min.css');
//require('./components/real-datatables.js');
console.log('Hello Webpack Encore');