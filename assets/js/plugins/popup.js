( function ($) {

    /**
     *
     * @param options
     * @returns {jQuery}
     */
    $.fn.pup = function (options) {

        var settings = $.extend({
            titre: '',
            hauteur: 200,
            largeur: 400,
            params: {},
            boutons: {},
            message: 'Vous ne pouvez pas faire ceci',
            action: 'creer',
            modal: false,
            resizable: false,
            containment: true,
            position: ['center',50]
        }, options);

        /**
         * Créer la popup
         */
        if (settings.action === 'creer') {
            var that = this;
            settings.params = {
                show: {
                    effect: "explode",
                    duration: 500
                },
                hide: {
                    effect: "explode",
                    duration: 500
                },
                close: function (event, ui) {
                    that.html('');
                },
                modal: settings.modal,
                resizable: settings.resizable,
                autoOpen: false,
                position: settings.position,
                height: settings.hauteur,
                width: settings.largeur,
                title: settings.titre,
                buttons: settings.boutons
            };

            var content = $('<div/>').html(settings.message);
            this.append(content);

            // Générer
            this.dialog(settings.params);
            this.parent().css({position:"fixed"}).end().dialog('open');
        }

        return this;
    };
}( jQuery ));