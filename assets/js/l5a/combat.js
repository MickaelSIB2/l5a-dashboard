/**
 * Created by Mickaël on 28/04/2017.
 */
require('datatables');
require('datatables/media/css/jquery.dataTables.min.css');
require('../../css/l5a/combat.css');
require('../jcanvas.min');
import Custom from './custom';
const Pusher = require('pusher-js');

$(document).ready(function () {
    var numeroPNJ = 0;
    orderByInit();
    initNouveauCombattant();

    // Enable pusher logging - don't include this in production
    var pusher_exists = Pusher !== undefined;
    if (pusher_exists) {
        Pusher.logToConsole = true;

        var pusher = new Pusher('f71ce56623796f90896a', {
            cluster: 'eu',
            encrypted: true
        });

        var game = $('#combat-id').attr('data-game');
        var channel = pusher.subscribe('game-' + game);
        pusher.connection.bind('error', function (err) {
            if (err.error.data.code === 4004) {
                log('>>> detected limit error');
            }
        });
        channel.bind('send-tab', function (data) {
            alert(data.message);
        });
    }

    // Mettre à jour la table joueur pour les joueurs
    if ($('#table-combat-gm').length === 0) {
        channel.bind('maj-combat', function(data) {
            //$('.combat-bouton a').attr('data-content', data); // Peupler le bouton combat
            afficherCombat(data);
        });
    }

    // Définir les rangs de blessure des samurai
    $('.samurai').each(function () {
        var samurai = $(this);
        if (samurai.attr('data-terre') !== undefined) {
            var terre = samurai.attr('data-terre');
            var rangs = Custom.definirRangsdeBlessures(terre);
            for (var i = 0; i < rangs.length; i++) {
                samurai.attr('data-rang-' + parseInt(i+1), rangs[i]['marge']);
            }
            majEtat(samurai);
        }

        const canvas = $('#canvas');
        var context = canvas.getContext('2d');
    });

    $(document).on('click', '#btn-test-pusher', function () {
        $.ajax({
            url: chemins.trigger_pusher,
            type: 'post'
        });
    });

    $(document).on('click', '.enlever-combattant', function () {
        var class_combattant_1 = $(this).parents('tr').attr('class');
        var class_combattant = '.'+class_combattant_1.replace(' ', '.');
        enleverCombattant(class_combattant);
    });

    $(document).on('click', '.editer-combattant', function () {
        afficherConfigPNJ($(this));
    });

    $(document).on('change', '#table-combat-gm .initiative', function () {
        var that = $(this);
        var parent = $(that.parents('tr')[0]);
        var combattant_class = '.' + parent.attr('class').replace(' ', '.');
        $('#table-combat-all '+combattant_class + ' .initiative').attr('value', that.val());
        orderByInit();
        majTableauCombatPJ();
    });

    $(document).on('change', '#select-pnj', function () {
        selectPNJ($(this));
    });

    $(document).on('change', '.posture', function () {
        majPosture($(this));
    });

    $(document).on('click', '#ajouter-combattant', function (e) {
        ajouterCombattant($(this), numeroPNJ);
        numeroPNJ++;
    });

    $(document).on('change', '#table-combat-gm .blessure', function () {
        majBlessuresCombattant($(this));
    });

    $(document).on('change', '#modal-principale #terre', function () {
        var that = $(this);
        var nouvelle_terre = $(this).val();
        var rangs = Custom.definirRangsdeBlessures(nouvelle_terre);
        for (var i = 0; i < rangs.length; i++) {
            $('#modal-principale .rangs-blessures #rang-'+parseInt(i+1)).val(rangs[i]['marge']);
        }
    });

    $(document).on('click', '#finir-tour', function () {
        finirTour();
    })
});

function orderByInit() {
    var tables = $('table:not(#table-nouveau-combattant)');
    tables.each(function () {
        var tableBody = $($(this).find('tbody'));
        var alltrs = $(tableBody.find('tr'));
        tableBody.html('');
        var allInits = [];
        alltrs.each(function () {
            var tr = $(this);
            var init = parseInt($($(tr.find('.initiative'))).val());
            if (init !== undefined && ! isNaN(init)) {
                while (allInits[init] !== undefined) {
                    init++;
                }
                allInits[init] = tr;
            }
        });

		// Reconstruire les tableaux
        for (var i = allInits.length; i >= 0; i--) {

            if (allInits[i] !== undefined) {
                tableBody.append(allInits[i]);
            }
        }
    });
}

function enleverCombattant(row_class) {
    var temps = 500;
    $(row_class).each(function () {
        var that = $(this);
        that.hide(temps, function() {
            that.remove();
        })
    });
    majTableauCombatPJ();
}

function afficherConfigPNJ(element) {
    var tr = element.parents('tr.pnj');
    var params = {
        show: {
            effect: "explode",
            duration: 500
        },
        hide: {
            effect: "explode",
            duration: 500
        },
        close: function (event, ui) {
            $('#modal-principale').html('');
        },
        modal: false,
        resizable: true,
        height: 'auto',
        width: '1000px'
    };

    var bonus = {};
    $.each(tr.info(), function(key, value) {
        if (key.indexOf('bonus') !== -1) {
            var aOneBonus = key.split('-');
            if (aOneBonus.length === 4) {
                if (bonus[aOneBonus[2]] === undefined) {
                    bonus[aOneBonus[2]] = {};
                }
                bonus[aOneBonus[2]][aOneBonus[3]] = value;
            }
        }
    });

    var message = $('<div/>');
    message.html(
        '<div class="anneaux" style="display: inline-block">' +
            '<div class="col-md-6">'+
                '<label for="air">Air: </label>' +
                '<input id="air" name="air" value="'+tr.attr('data-air')+'" type="number">'+
            '</div>' +
            '<div class="col-md-6">' +
                '<label for="eau">Eau: </label>' +
                '<input id="eau" name="eau" value="'+tr.attr('data-eau')+'" type="number">' +
            '</div>' +
            '<div class="col-md-6">' +
                '<label for="terre">Terre: </label>' +
                '<input id="terre" name="terre" value="'+tr.attr('data-terre')+'" type="number">' +
            '</div>' +
            '<div class="col-md-6">' +
                '<label for="feu">Feu: </label>' +
                '<input id="feu" name="feu" value="'+tr.attr('data-feu')+'" type="number">' +
            '</div>' +
            '<div class="col-md-6">' +
                '<label for="vide">Vide: </label>' +
                '<input id="vide" name="vide" value="'+tr.attr('data-vide')+'" type="number">' +
            '</div>' +
            '<div class="col-md-6">' +
                '<label for="honneur">Honneur : </label>' +
                '<input id="honneur" name="honneur" value="'+tr.attr('data-honneur')+'" type="number">' +
            '</div>' +
            '<div class="col-md-6">' +
                '<label for="rang-maitrise">Rang de Maîtrise : </label>' +
                '<input id="rang-maitrise" name="rang-maitrise" value="'+tr.attr('data-rang-maitrise')+'" type="number">' +
            '</div>' +
            '<div class="col-md-6">' +
                '<label for="jet-init">Initiative : </label>' +
                '<input id="jet-init" name="jet-init" class="preparation-jet" value="'+tr.attr('data-jet-init')+'" type="text">' +
            '</div>' +
        '</div>' +
        '<div class="rangs-blessures" style="display: inline-block">' +
            '<div class="col-md-4">' +
                '<label for="rang-1">Rang 1</label>' +
                '<input id="rang-1" name="rang-1" value="'+tr.attr('data-rang-1')+'">' +
            '</div>' +
            '<div class="col-md-4">' +
                '<label for="rang-2">Rang 2</label>' +
                '<input id="rang-2" name="rang-2" value="'+tr.attr('data-rang-2')+'">' +
            '</div>' +
            '<div class="col-md-4">' +
                '<label for="rang-3">Rang 3</label>' +
                '<input id="rang-3" name="rang-3" value="'+tr.attr('data-rang-3')+'">' +
            '</div>' +
            '<div class="col-md-4">' +
                '<label for="rang-4">Rang 4</label>' +
                '<input id="rang-4" name="rang-4" value="'+tr.attr('data-rang-4')+'">' +
            '</div>' +
            '<div class="col-md-4">' +
                '<label for="rang-5">Rang 5</label>' +
                '<input id="rang-5" name="rang-5" value="'+tr.attr('data-rang-5')+'">' +
            '</div>' +
            '<div class="col-md-4">' +
                '<label for="rang-6">Rang 6</label>' +
                '<input id="rang-6" name="rang-6" value="'+tr.attr('data-rang-6')+'">' +
            '</div>' +
            '<div class="col-md-4">' +
                '<label for="rang-7">Rang 7</label>' +
                '<input id="rang-7" name="rang-7" value="'+tr.attr('data-rang-7')+'">' +
            '</div>' +
            '<div class="col-md-4">' +
                '<label for="rang-8">Rang 8</label>' +
                '<input id="rang-8" name="rang-8" value="'+tr.attr('data-rang-8')+'">' +
            '</div>' +
        '</div>' +
        '<div class="description">'+
            '<table>'+
                '<thead>' +
                    '<tr>' +
                        '<th>Type</th>'+
                        '<th>Nom</th>'+
                        '<th>Description</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody></tbody>' +
            '</table>' +
        '</div>');


    $('#modal-principale').append(message);

    if (bonus !== undefined) {
        $.each(bonus, function () {
            var value = $(this)[0];

            if (value.type === undefined) {
                value.type = '';
            }
            var new_row =
                '<tr>'+
                '<td>'+value.type+'</td>'+
                '<td>'+value.nom+'</td>'+
                '<td>'+value.description+'</td>'+
                '</tr>'
            ;
            $('div.description tbody').append(new_row);
        });
    }

    params.title = "Édition du PNJ";
    params.buttons = {
        "Enregistrer": function() {
            var
                air = $('#modal-principale #air').val(),
                eau = $('#modal-principale #eau').val(),
                terre = $('#modal-principale #terre').val(),
                feu = $('#modal-principale #feu').val(),
                vide = $('#modal-principale #vide').val(),
                honneur = $('#modal-principale #honneur').val(),
                rang_1 = $('#modal-principale #rang-1').val(),
                rang_2 = $('#modal-principale #rang-2').val(),
                rang_3 = $('#modal-principale #rang-3').val(),
                rang_4 = $('#modal-principale #rang-4').val(),
                rang_5 = $('#modal-principale #rang-5').val(),
                rang_6 = $('#modal-principale #rang-6').val(),
                rang_7 = $('#modal-principale #rang-7').val(),
                rang_8 = $('#modal-principale #rang-8').val(),
                description = $('#modal-principale #description').val(),
                bonus = $('#modal-principale #bonus').val(),
                jet_init = $('#modal-principale #jet-init').val();
            tr.attr(
                {
                    'data-air': air,
                    'data-eau': eau,
                    'data-terre': terre,
                    'data-feu': feu,
                    'data-vide': vide,
                    'data-honneur': honneur,
                    'data-rang-1': rang_1,
                    'data-rang-2': rang_2,
                    'data-rang-3': rang_3,
                    'data-rang-4': rang_4,
                    'data-rang-5': rang_5,
                    'data-rang-6': rang_6,
                    'data-rang-7': rang_7,
                    'data-rang-8': rang_8,
                    'data-description': description,
                    'data-bonus': bonus,
                    'data-jet-init': jet_init
                });
            $(this).dialog( "close" );
        },
        "Annuler": function() {
            $(this).dialog( "close" );
        }
    };
    $('#modal-principale').dialog(params);
};

function majEtat(combattant) {
    var blessure = parseInt($(combattant.find('.blessure')).val());
    var classCombattant = '.'+combattant.attr('class').replace(' ', '.');
    var
        rang_1 = parseInt(combattant.attr('data-rang-1')),
        rang_2 = parseInt(combattant.attr('data-rang-2')),
        rang_3 = parseInt(combattant.attr('data-rang-3')),
        rang_4 = parseInt(combattant.attr('data-rang-4')),
        rang_5 = parseInt(combattant.attr('data-rang-5')),
        rang_6 = parseInt(combattant.attr('data-rang-6')),
        rang_7 = parseInt(combattant.attr('data-rang-7')),
        rang_8 = parseInt(combattant.attr('data-rang-8'));

    var color = '';
    if (blessure > rang_1) {
        if ($.isNumeric(rang_2)) {
            // color = '#7f8778';
            color = '#E0FF00';
        } else {
            enleverCombattant(classCombattant);
        }
    }
    if (blessure > rang_2) {
        if ($.isNumeric(rang_3)) {
            // color = '#66c88f';
            color = '#E0FF00';
        } else {
            enleverCombattant(classCombattant);
        }
    }
    if (blessure > rang_3) {
        if ($.isNumeric(rang_4)) {
            // color = '#d2ca11';
            color = '#FFC000';
        } else {
            enleverCombattant(classCombattant);
        }
    }
    if (blessure > rang_4) {
        if ($.isNumeric(rang_5)) {
            // color = '#ce520f';
            color = '#FFC000';
        } else {
            enleverCombattant(classCombattant);
        }
    }
    if (blessure > rang_5) {
        if ($.isNumeric(rang_6)) {
            // color = '#c12e2e';
            color = '#FF3000';
        } else {
            enleverCombattant(classCombattant);
        }
    }
    if (blessure > rang_6) {
        if ($.isNumeric(rang_7)) {
            // color = '#9c0e0e';
            color = '#FF3000';
        } else {
            enleverCombattant(classCombattant);
        }
    }
    if (blessure > rang_7) {
        if ($.isNumeric(rang_8)) {
            // color = '#763939';
            color = '#FF3000';
        } else {
            enleverCombattant(classCombattant);
        }
    }
    if (blessure >= rang_8) {
        enleverCombattant(classCombattant);
    } else {
        $(classCombattant).each(function () {
            var that = $(this);
            if (that.attr('data-terre') === undefined) {
                $(that.find('.blessure')).html(blessure);
            }
            that.attr('style', 'background-color:'+ color);
        });
    }
};

function selectPNJ(element) {
	var valeur = element.val();
	var url = chemins.select_pnj;
	$.ajax({
		type: 'post',
		url: url,
		data: { 'id': valeur },
		success: function(response) {
			var tr = $(element.parents('.pnj')[0]);
			var pnj = response.pnj;
			var bonus = $.makeArray(response.bonus);

			$(tr.find('.nom')).val(pnj.nom);
			$(tr.find('.jet-attaque')).val(pnj.jetAttaque);
			$(tr.find('.jet-degats')).val(pnj.jetDegats);
			$(tr.find('.nd')).val(pnj.nd);
			$(tr.find('.reduction')).val(pnj.reduction);
			$(tr.find('.arme')).val(pnj.arme);
			$(tr.find('.armure')).val(pnj.armure);
			var rang_maitrise = pnj.rangMaitrise !== null ? parseInt(pnj.rangMaitrise) : 1;
			var init = ( rang_maitrise + parseInt(pnj.reflexes)).toString() + 'G' + pnj.reflexes;

			var attributs = {
                'data-air': pnj.air,
                'data-eau': pnj.eau,
                'data-terre': pnj.terre,
                'data-feu': pnj.feu,
                'data-vide': pnj.vide,
                'data-honneur': pnj.honneur,
                'data-rang-1': pnj.rangBlessure1,
                'data-rang-2': pnj.rangBlessure2,
                'data-rang-3': pnj.rangBlessure3,
                'data-rang-4': pnj.rangBlessure4,
                'data-rang-5': pnj.rangBlessure5,
                'data-rang-6': pnj.rangBlessure6,
                'data-rang-7': pnj.rangBlessure7,
                'data-rang-8': pnj.rangBlessure8,
                'data-description': pnj.notes,
                'data-jet-init': init,
                'data-rang-maitrise': pnj.rangMaitrise
            };

			for(var i = 1; i < bonus.length + 1; i++) {
			    var nom_attr = 'data-bonus-'+i;
			    attributs[nom_attr+'-type'] = bonus[i-1]['type'];
			    attributs[nom_attr+'-nom'] = bonus[i-1]['nom'];
			    attributs[nom_attr+'-description'] = bonus[i-1]['description'];
            }
			tr.attr(attributs);
		}
	})
}

function majTableauCombatPJ() {
    // Si le premier tour n'est pas defini, ne pas envoyer le tableau
    if ($('.tour-actuel').length === 0) {
        return;
    }
    var tableau = $('#table-combat-all').html();
    var url = chemins.maj_tableau_combat_pj;
    $.ajax({
        url: url,
        type: 'post',
        data: { 'html': tableau },
        success: function (response) {
            console.log(response.message);
        },
        fail: function(response) {
            alert('Le tableau n\'a pas pu être mis à jour pour les joueurs');
        }
    })
}

function majPosture(posture) {
    var combattant = posture.parents('tr');
    var classCombattant = '.'+combattant.attr('class').replace(' ', '.');
    $('#table-combat-all ' + classCombattant + ' .posture').text(posture.val());
    majTableauCombatPJ();
}

function ajouterCombattant(combattant, numeroPNJ) {
    var row = $(combattant.parents('.pnj'));
    var initiative = $(row.find('.initiative')),
        nom = $(row.find('.nom')),
        jet_attaque = $(row.find('.jet-attaque')),
        jet_degats = $(row.find('.jet-degats')),
        nd = $(row.find('.nd')),
        reduction = $(row.find('.reduction')),
        arme = $(row.find('.arme')),
        armure = $(row.find('.armure'))
    ;
    var tbody_gm = $('#table-combat-gm tbody');
    var nouveau_combattant_gm = $('<tr/>');

    if (row.attr('data-vide') !== undefined) {
        nouveau_combattant_gm.attr(row.info());
    } else {
        nouveau_combattant_gm.attr(
            {
                'data-air': 2,
                'data-eau': 2,
                'data-terre': 2,
                'data-feu': 2,
                'data-vide': 2,
                'data-honneur': 3,
                'data-rang-1': 10,
                'data-rang-2': 14,
                'data-rang-3': 18,
                'data-rang-4': 22,
                'data-rang-5': 26,
                'data-rang-6': 30,
                'data-rang-7': 34,
                'data-rang-8': 38,
                'data-description': '',
                'data-bonus': '',
                'data-jet-init': '3G2',
                'data-rang-maitrise': 1
            });
    }
    nouveau_combattant_gm.attr('class', 'pnj ' + numeroPNJ);

    nouveau_combattant_gm.html(
        '<td class="tour"></td>'+
        '<td><input class="initiative" value="'+initiative.val()+'" type="number"></td>'+
        '<td><input class="nom" value="'+nom.val()+'"></td>'+
        '<td><a href="javascript:void(0)" class="jet-attaque bouton-preparation-lancer" data-jet="'+jet_attaque.val()+'">'+jet_attaque.val()+'</a></td>'+
        '<td><a href="javascript:void(0)" class="jet-degats bouton-preparation-lancer" data-jet="'+jet_degats.val()+'">'+jet_degats.val()+'</a></td>'+
        '<td><input class="nd" value="'+nd.val()+'"></td>'+
        '<td><input class="reduction" value="'+reduction.val()+'"></td>'+
        '<td><input class="arme" value="'+arme.val()+'"></td>'+
        '<td><input class="armure" value="'+armure.val()+'"></td>'+
        '<td><input class="blessure" value="0" type="number" min="0"></td>'+
        '<td>'+
        '<select class="posture">'+
        '<option value="attaque">Attaque</option>'+
        '<option value="assaut">Assaut</option>'+
        '<option value="defense">Défense</option>'+
        '<option value="esquive">Esquive</option>'+
        '<option value="centre">Centre</option>'+
        '</select>'+
        '</td>'+
        '<td>' +
        '<button class="enlever-combattant">Enlever</button>' +
        '<button class="editer-combattant">Éditer</button>' +
        '</td>'
    );
    tbody_gm.append(nouveau_combattant_gm);

    // Mettre à jour le tableau des joueurs
    var tbody_joueurs = $('#table-combat-all tbody');
    var nouveau_combattant_joueurs = $('<tr/>');
    nouveau_combattant_joueurs.attr('class', 'pnj '+numeroPNJ);
    nouveau_combattant_joueurs.html(
        '<td class="tour"></td>'+
        '<td class="hidden"><input class="initiative" value="'+initiative.val()+'" disabled="true" type="number"/></td>'+
        '<td><input class="nom" value="'+nom.val()+'" disabled="true" type="text"></td>'+
        '<td><input class="arme" value="'+arme.val()+'" disabled="true" type="text"></td>'+
        '<td><input class="armure" value="'+armure.val()+'" disabled="true" type="text"></td>'+
        // '<td class="blessure"></td>'+
        '<td class="posture">Attaque</td>'
    );
    tbody_joueurs.append(nouveau_combattant_joueurs);
    orderByInit();
    majTableauCombatPJ();
}

function majBlessuresCombattant(element) {
    var combattant = element.parents('tr');
    majEtat(combattant);
    if (combattant.hasClass('samurai')) {
        var chemin = combattant.attr('data-chemin');
        var blessure = $(combattant.find('.blessure')).val();
        $.ajax({
            url: chemin,
            method: 'post',
            data: {
                champ: 'Blessures',
                valeur: blessure,
                provenance: 'combat'
            },
            fail: function (response) {
                console.log('Les blessures n\'ont pas pu être enregistrées');
            }
        });
    };
    majTableauCombatPJ();
}

/**
 * Cette fonction permet de définir le premier tour s'il n'est pas défini et démarrer le combat
 * et à définir la fin d'un tour et le début du suivant
 */
function finirTour() {
    var tables = $('#table-combat-gm tbody, #table-combat-all tbody');
    var tour_actuel = $('<div/>')
        .addClass('tour-actuel')
        .html('&#10163;');

    // Si le premier tour n'a pas encore ete defini, l'attribuer au premier joueur
    if ($('.tour-actuel').length === 0) {
        tables.each(function () {
            debutTour($(this), tour_actuel);
        });
        $('#finir-tour').text('Terminer le tour');
    } else {
        // Joueur suivant
        $('.tour .tour-actuel').each(function(){
            var that = $(this);
            var table = $(that.parents('tbody'));
            var combattant_suivant = $(that.parents('tr')).next();
            that.remove();
            if (combattant_suivant.length > 0) {
                tour_actuel.clone().appendTo($($(combattant_suivant).find('.tour')));
            } else {
                debutTour(table, tour_actuel);
            }
         });
    }
    majTableauCombatPJ();
};

function debutTour(table, tour_actuel) {
    var tour = $($(table.children(0)).find('.tour')).first();
    tour_actuel.clone().appendTo(tour);
};

function initNouveauCombattant() {
    $('#table-nouveau-combattant input').each(function () {
        var that = $(this);
        if (that.hasClass('initiative')) {
            that.val(0);
        } else {
            that.val('');
        }
    });
}

$.fn.info = function () {
    var data = {};
    [].forEach.call(this.get(0).attributes, function (attr) {
        if (/^data-/.test(attr.name)) {
            var camelCaseName = attr.name.substr(5);
            data['data-'+camelCaseName] = attr.value;
        }
    });
    return data;
};

function afficherCombat(data) {
    if ($('#table-combat-all').length > 0) {
        $('#combat-all').html('');
    }
    var content = $('<table/>').attr({
        'id': 'table-combat-all'
    });
    content.append(data);

    $('#combat-all').append(content);
}
