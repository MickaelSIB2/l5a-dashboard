//const toastr = require('toastr');
import Custom from './custom';
require('datatables');
require('datatables/media/css/jquery.dataTables.min.css');
//import Translator from 'bazinga-translator';

// const toastr = require('toastr');
const Pusher = require('pusher-js');
require ('../../css/l5a/character-sheet.css');

/////// Page samurai ///////////

var samurai_id = $('#id-samurai').attr('data-id-samurai');
var game = $('#details-principaux').attr('data-game');

// Enable pusher logging - don't include this in production
var pusher_exists = Pusher !== undefined;
// var pusher_exists = false;
if (pusher_exists) {
    Pusher.logToConsole = true;

    /*var pusher = new Pusher('f71ce56623796f90896a', {
        cluster: 'eu',
        encrypted: true
    });*/

    var pusher = new Pusher(pusher_key, {
        cluster: pusher_cluster,
    });

    var channel = pusher.subscribe('game-' + game);
    pusher.connection.bind('error', function (err) {
        if (err.error.data.code === 4004) {
            log('>>> detected limit error');
        }
    });
}

$('#samurai-competences-table.datatable').DataTable({
    // scrollY: 1000
    // columns.editField
    'order': [0, 'asc'],
    'bPaginate': false,
    autoWidth: false,
    'aoColumns': [
        null,
        { "bSearchable": false },
        null,
        { "bSearchable": false },
        null,
        null,
        null,
        { "bSearchable": false },
        { "bSearchable": false }
    ]
});

if (pusher_exists) {
    channel.bind('maj-blessures-' + samurai_id, function (blessures) {
        $('#blessures').val(blessures);
        majBlessures();
    });

    channel.bind('maj-provisions', function (data) {
        console.log(data);
        $('#provisions').val(data);
    });

    channel.bind('maj-notes-joueurs', function (data) {
        console.log(data);
        $('#notes-joueurs').val(data);
    });
}
initArmes();
// Combat
//Init
initInitiative();

// ND Armure
initNDArmure();

// Blessures
initBlessures();

// Récupération
initRecuperation();

$(document).on('click', '.technique', function () {
    afficherTechniquesDuRang($(this));
});

/*$(document).on('click', '.combat-bouton a', function () {
    var that = $(this);
    var data = that.attr('data-content');
    afficherCombat(data);
});*/

$(document).on('change', '.toggle-vues', function () {
    toggleVues($(this));
});

$(document).on('keyup', '.edit-personnage', function() {
    majPersonnage($(this), false)
});

$(document).on('keyup', '.edit-lieu', function() {
    majLieu($(this), false)
});

$(document).on('keyup', '#objectifs', function() {
    majObjectifs($(this))
});

$(document).on('keyup', '#init-modificateurs', function () {
    majBonusInit();
});

$(document).on('change', '#nd-modificateurs', function () {
    majBonusNDArmure();
});

$(document).on('change', '#blessures', function () {
    majBlessures();
});

$(document).on('change', '#recuperation-modificateurs', function () {
    majBonusRecuperation();
});

$(document).on('change', '.arme .choix-fleches', function () {
    switchFleche($(this).parents('.arme'));
});

$(document).on('change', '.arme .qte-restante', function () {
    majFleches($(this));
});

$(document).on('change', '.edit-samurai', function() {
    majSamurai($(this));
});

$(document).on('click', '.supprimer-personnage', function() {
    supprimerPersonnage($(this));
});

$(document).on('click', '.ajouter-personnage', function() {
    majPersonnage($(this), true);
});

$(document).on('click', '.ajouter-lieu', function() {
    majLieu($(this), true);
});

$(document).on('click', '.ajouter-item', function() {
    nouvelItem();
});

$(document).on('change', '.maj-monnaie', function() {
    var that = $(this);
    var monnaie = that.attr('id').replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); });
    var valeur = that.val();
    majMonnaie(monnaie, valeur);
});

// Cacher / Montrer les champs editables
$(window).click(function () {
    $('.to-edit').each(function () {
        if ($(this).css('display') !== 'none') {
            $(this).hide();
        }
    });

    $('.editable').each(function () {
        if ($(this).css('display') === 'none') {
            $(this).show();
        }
    });
});

$(document).on('click', '.to-edit', function (event) {
    event.stopPropagation();
});

$(document).on('click', '.editable', function (event) {
    event.stopPropagation();
    var that = $(this);
    that.hide();
    that.next('.to-edit').show();
});

$(document).on('change', '.to-edit input, .to-edit textarea', function (event) {
    majArmePerso($(this));
});

$(document).on('click', '.equiper-armure', function () {
    equiperArmure($(this));
});

$(document).on('click', '.sort-appris', function() {
    afficherSort($(this));
});

$(document).on('click keyup change', '#honneur-points, #gloire-points', function() {
    var that = $(this);
    majRangs(that);
});

$(document).on('click', '.supprimer-equipement', function() {
    supprimerEquipement($(this));
});

$(document).on('click', '.supprimer-arme', function() {
    supprimerArme($(this));
});

$(document).on('change', '.resume-avantage', function() {
    editerAvantage($(this), 'resume');
});

$(document).on('change', '.est-cache-avantage', function() {
    editerAvantage($(this), 'cache');
});

$(document).on('change', '.resume-desavantage', function() {
    editerDesavantage($(this), 'resume');
});

$(document).on('change', '.est-cache-desavantage', function() {
    editerDesavantage($(this), 'cache');
});

$(document).on('change', '#provisions', function () {
    maj_provisions($(this));
});

$(document).on('change', '#notes-joueurs', function () {
    maj_notes_joueurs($(this));
});

// Changer de technqiue
$(document).on('click', '.technique_a_choisir', function() {
    var technique_selectionnee = $(this);
    if(! technique_selectionnee.hasClass('active')) {
        $(technique_selectionnee.siblings('.technique_a_choisir')).each(function() {
            $(this).removeClass('active');
        });
        technique_selectionnee.addClass('active');
    }
});

function initArmes() {
    $('.arme').each(function() {
        var arme = $(this);
        var jet = arme.find('.jet');
        var filtre_attaque = jet.data('filtre');
        var type = arme.find('.type');
        var degats = arme.find('.jet-degats');
        var filtre_degats = degats.data('filtre');

        // Initialiser les input des filtres
        var aFiltre_attaque = Custom.splitG(filtre_attaque);
        jet.next().find('.filtre-lancer').val(parseInt(aFiltre_attaque[0]));
        jet.next().find('.filtre-garder').val(parseInt(aFiltre_attaque[1]));

        var aFiltre_degats = filtre_degats.split('G');
        degats.next().find('.filtre-lancer').val(parseInt(aFiltre_degats[0]));
        degats.next().find('.filtre-garder').val(parseInt(aFiltre_degats[1]));
        ///

        var is_arc = type.text() === Translator.trans('arme.arc');
        if (is_arc) {
            switchFleche(arme);
        } else {
            var jet_degats = getDegats(degats.data('vd'), 0, filtre_degats);
            degats.text(jet_degats[1]);
        }

        var force = (is_arc) ? arme.find('.arme-force') : 0;

        var competence = 'pas-abilite',
            trait; // pas-abilite est un id qui n'existe pas dans la page,
        // ce qui se réfèrera plus tard par un 0

        switch (type.text()) {

            default:
                competence = 'arme.spear';
                trait = 'agilite';
                break;

            case Translator.trans('arme.sword'):
                competence = 'Kenjutsu';
                trait = 'agilite';
                break;

            case Translator.trans('arme.arc'):
                competence = 'Kyujutsu';
                trait = 'reflexes';
                break;

            case Translator.trans('arme.chain'):
                competence = 'Armes-à-chaîne';
                trait = 'reflexes';
                break;

            case Translator.trans('arme.hast'):
                competence = 'Hast';
                trait = 'agilite';
                break;

            case Translator.trans('arme.arme_lourde'):
                competence = 'Armes Lourdes';
                trait = 'agilite';
                break;

            case Translator.trans('arme.staff'):
                competence = 'Bâtons';
                trait = 'agilite';
                break;

            case Translator.trans('arme.knife'):
                competence = 'Couteaux';
                trait = 'agilite';
                break;

            case Translator.trans('arme.fan'):
                competence = 'Eventail';
                trait = 'agilite';
                break;

            case Translator.trans('arme.spear'):
                competence = 'Lances';
                trait = 'agilite';
                break;
        }

        var nouveau_jet = getJet(competence, trait, filtre_attaque);
        jet.text(nouveau_jet[1]);
        jet.attr('data-jet-generique', nouveau_jet[0]);

    });
}

function majPersonnage(element, is_new) {
    is_new = is_new || false;

    var id, nom, rapport, notes;
    var notesCampagne = $('#notes-campagne.card');

    if (is_new === false) {
        id = element.data('personnage-id');
        nom = $('.personnage-' + id + '.personnage-nom').val();
        rapport = $('.personnage-' + id + '.personnage-rapport').val();
        notes = $('.personnage-' + id + '.personnage-notes').val();
    } else {
        id = null;
        nom = $('.new-personnage-nom').val();
        rapport = $('.new-personnage-rapport').val();
        notes = $('.new-personnage-notes').val();
    }

    if (nom !== '') {
        if (is_new === true) {
            notesCampagne.html('');
        }
        $.ajax({
            url: chemins.majPersonnage,
            method: 'post',
            data: {
                id: id,
                nom: nom,
                rapport: rapport,
                notes: notes,
                is_new: is_new
            },
            success: function (response) {
                if (is_new === true) {
                    notesCampagne.html(response.content);
                }
                toastr["success"](Translator.trans('toast.edit_pc.success', {'name': nom}));
            },
            error: function () {
                toastr["error"](Translator.trans('toast.edit_pc.failure', {'name': nom}));
            }
        })
    }
}

function majLieu(element, is_new) {
    is_new = is_new || false;

    var id, nom;
    var notesCampagne = $('#notes-campagne.card');

    if (is_new === false) {
        id = element.data('lieu-id');
        nom = $('.lieu-' + id).val();
    } else {
        id = null;
        nom = $('.new-lieu').val();
    }

    if (nom !== '') {
        if (is_new === true) {
            notesCampagne.html('');
        }
        $.ajax({
            url: chemins.majLieu,
            method: 'post',
            data: {
                id: id,
                nom: nom,
                is_new: is_new
            },
            success: function (response) {
                if (is_new === true) {
                    notesCampagne.html(response.content);
                }
                toastr["success"]("Le lieu " + nom + " a bien pu être édité.");
            },
            error: function () {
                toastr["error"](Translator.trans('toast.edit_location.failure', {'name': nom}));
            }
        })
    }
}

function majObjectifs(element) {
    var objectifs = element.val();

    $.ajax({
        url: chemins.majObjectifs,
        method: 'post',
        data: {
            objectifs: objectifs
        },
        success: function (response) {
            // toastr["success"]("Objectifs mis à jour.");
        },
        error: function () {
            toastr["error"](Translator.trans('toast.objectives.failure'));
        }
    })
}

function majBonusInit() {
    var modificateur = $('#init-modificateurs').val();

    $.ajax({
        url: chemins.majSamurai,
        method: 'post',
        data: {
            champ: 'BonusInit',
            valeur: modificateur
        },
        success: function (response) {
            initInitiative();
            toastr["success"](Translator.trans('toast.initiative.success'));
        },
        error: function () {
            toastr["error"](Translator.trans('toast.initiative.failure'));
        }
    })
}

function majBonusNDArmure() {
    var modificateur = $('#nd-modificateurs').val();

    $.ajax({
        url: chemins.majSamurai,
        method: 'post',
        data: {
            champ: 'BonusND',
            valeur: modificateur
        },
        success: function (response) {
            initNDArmure();
            toastr["success"](Translator.trans('toast.nd.success'));
        },
        error: function () {
            toastr["error"](Translator.trans('toast.nd.failure'));
        }
    })
}

function majBlessures() {
    var blessures = $('#blessures').val();

    $.ajax({
        url: chemins.majSamurai,
        method: 'post',
        data: {
            champ: 'Blessures',
            valeur: blessures
        },
        success: function (response) {
            initBlessures();
        },
        error: function () {
            toastr["error"](Translator.trans('toast.wounds.failure'));
        }
    })
}

function majBonusRecuperation() {
    var modificateur = $('#recuperation-modificateurs').val();

    $.ajax({
        url: chemins.majSamurai,
        method: 'post',
        data: {
            champ: 'BonusRecup',
            valeur: modificateur
        },
        success: function (response) {
            initRecuperation();
            toastr["success"](Translator.trans('toast.recuperation.success'));
        },
        error: function () {
            toastr["error"](Translator.trans('toast.recuperation.failure'));
        }
    })
}

// Calcule les dégâts en fonction de la flèche
function switchFleche(arme) {
    var choixFleche = arme.find('.choix-fleches option:selected');
    var vd = choixFleche.attr('data-vd');
    var notes = choixFleche.attr('data-notes');
    var qte = choixFleche.attr('data-qte');
    var id = choixFleche.attr('data-id');
    var degats_inputs = arme.find('td.calque-degats input');
    var value = $(degats_inputs[0]).val() + 'G' + $(degats_inputs[1]).val(); // filtre à sauvegarder

    var arcForce = parseInt(arme.find('.arme-force').text());
    arme.find('.qte-restante').val(qte).attr('data-id', id);
    var nouveaux_degats = getDegats(vd, arcForce, value)[1];
    arme.find('.jet-degats').text(nouveaux_degats).data('vd', vd);
    arme.find('.notes').text(arme.find('.notes').text() + "\n" + notes);
    arme.find('.bouton-preparation-lancer.degats').attr('data-jet', nouveaux_degats);
}

function majFleches(element) {
    var that = element;
    var id = that.attr('data-id');
    var qte = that.val();

    $.ajax({
        url: chemins.majFleches,
        method: 'post',
        data: {
            id: id,
            qte: qte
        },
        success: function (response) {
            $('.choix-fleches option[data-id="'  + id + '"]').attr('data-qte', qte);
            toastr["success"](Translator.trans('toast.arrows.success'));
        },
        error: function () {
            toastr["error"](Translator.trans('toast.arrows.failure'));
        }
    })
}

function majSamurai(element) {
    var isTextarea = element.data('textarea');
    var champ = element.data('champ');
    var valeur = '';
    var hgs = $('#honneur-gloire-et-souillure');
    switch(champ) {
        case 'Honneur':
            valeur = ($('#honneur-rang').val()).toString() + '.' + ($('#honneur-points').val()).toString();
            break;
        case 'Gloire':
            valeur = ($('#gloire-rang').val()).toString() + '.' + ($('#gloire-points').val()).toString();
            break;
        default:
            valeur = (isTextarea === 'true') ? element.text() : element.val();
            break;
    }

    $.ajax({
        url: chemins.majSamurai,
        method: 'post',
        data: {
            champ: champ,
            valeur: valeur
        },
        success: function (response) {
            // if (champ === 'Honneur' || champ === 'Gloire') {
            //     console.log(response.html);
            //     hgs.html(response.html);

            toastr["success"](Translator.trans('toast.edit.success', {'field': champ}));
            return true;
        },
        error: function () {
            toastr["error"](Translator.trans('toast.edit.failure', {'field': champ}));
        }
    })
}

function majArmePerso(element) {
    var type = element.data('type');
    if (type.substr(0, 4) === 'arme') {
        var url = chemins.majArmePerso,
            champ = '',
            complete = null,
            value = element.val(),
            type_arme = element.attr('data-type-arme'),
            arme = $(element.parents('.arme')),
            id = element.parents('.arme').data('id');
        switch (type) {
            case 'arme-nom':
                champ = 'Nom';
                complete = function (response) {
                    element.parent().prev().text(value);
                };
                break;
            case 'arme-notes':
                champ = 'Notes';
                complete = function (response) {
                    var editable = element.parent().prev();
                    var notes_arme_generique = editable.data('notes-generiques');
                    editable.text(notes_arme_generique + "\n" + value);
                };
                break;
            case 'arme-jet':
                champ = 'FiltreAttaque';
                var jet_inputs = element.parent().find('input');
                value = $(jet_inputs[0]).val() + 'G' + $(jet_inputs[1]).val(); // filtre à sauvegarder
                complete = function (response) {
                    // Mettre à jour
                    var jet_actuel = element.parent().prev().data('jet-generique');
                    var aJet_actuel = jet_actuel.split('G');
                    var nouveau_jet = (parseInt(aJet_actuel[0]) + parseInt($(jet_inputs[0]).val())).toString() + 'G' + (parseInt(aJet_actuel[1]) + parseInt($(jet_inputs[1]).val())).toString();
                    element.parent().prev().text(nouveau_jet);
                    $(element.parents('.arme').find('.bouton-preparation-lancer.attaque')).attr('data-jet', nouveau_jet);
                };
                break;
            case 'arme-degats':
                champ = 'FiltreDegats';
                var degats_inputs = element.parent().find('input');
                value = $(degats_inputs[0]).val() + 'G' + $(degats_inputs[1]).val(); // filtre à sauvegarder
                complete = function (response) {
                    // Mettre à jour
                    if (type_arme === 'arc') {
                        switchFleche(arme);
                    } else {
                        var degats_actuel = element.parent().prev().data('vd');
                        // var aDegats_actuels = degats_actuel.split('G');
                        // var nouveaux_degats = (parseInt(aDegats_actuels[0]) + parseInt($(degats_inputs[0]).val())).toString() + 'G' + (parseInt(aDegats_actuels[1]) + parseInt($(degats_inputs[1]).val())).toString();
                        var nouveaux_degats = getDegats(degats_actuel, 0, value)[1];
                        element.parent().prev().text(nouveaux_degats);
                    }

                    arme.find('.bouton-preparation-lancer.degats').attr('data-jet', nouveaux_degats);
                };
                break;
        }
    }

    // Send stuff away
    $.ajax({
        url: url,
        type: 'post',
        data: {
            champ: champ,
            valeur: value,
            id: id
        },
        success: function (response) {
            complete(response);
        },
        error: function () {
            toastr["error"](Translator.trans('toast.error'));
        }
    })
}

function equiperArmure(element) {
    var id_armure = element.attr('data-id-armure');
    var id_armure_equipee = element.attr('data-id-armure-equipee');

    if (id_armure == id_armure_equipee) {
        return;
    }

    var url = chemins.equiperArmure;
    $.ajax({
        url: url,
        method: 'post',
        data: {
            id_armure: id_armure
        },
        success: function(reponse) {
            $('#equipement .equiper-armure').each(function() {
                var that = $(this);
                that.attr('data-id-armure-equipee', id_armure);
            });
            var armure = reponse.armure;
            $('#armure-nom').text(armure.nom);
            $('#armure-nd').text(armure.nd);
            $('#armure-reduction').text(armure.reduction);
            initNDArmure();
            toastr["success"](Translator.trans('toast.equip_armor.success'));
        },
        error: function () {
            toastr["error"](Translator.trans('toast.equip_armor.failure'));
        }
    })
}

function afficherSort(element) {
    var nom = element.attr('data-nom'),
        anneau = element.attr('data-anneau'),
        maitrise = element.attr('data-maitrise'),
        mots_cles = element.attr('data-mots-cles'),
        portee = element.attr('data-portee'),
        zone_effet = element.attr('data-zone-effet'),
        duree = element.attr('data-duree'),
        augmentations = element.attr('data-augm'),
        description = element.attr('data-description'),
        anneau_samurai = element.attr('data-anneau-samurai'),
        rang_samurai = element.attr('data-rang-samurai'),
        des_lances = 0,
        jet = '';

    if ($('#details-sort').length > 0) {
        $('#notes-perso-modal').html('');
    }

    var message = $('<div/>').attr({
        'id': 'details-sort',
        'style': 'overflow-y: auto;'
    });
    message.html(
        '<div class="col-md-12 mb"><strong>'+ Translator.trans('Ring') + '/' + Translator.trans('Mastery') +
        ': </strong>'+anneau+'/'+maitrise+' ('+mots_cles+')</div>'+
        '<div class="col-md-12 mb"><strong>'+ Translator.trans('Reach') +': </strong>'+portee+'</div>'+
        '<div class="col-md-12 mb"><strong>'+ Translator.trans('spell.aoe') +': </strong>'+zone_effet+'</div>'+
        '<div class="col-md-12 mb"><strong>'+ Translator.trans('spell.duration') +': </strong>'+duree+'</div>'+
        '<div class="col-md-12 mb"><strong>'+ Translator.trans('Augmentations') +': </strong>'+augmentations+'</div>'+
        '<div class="col-md-12 mb"><strong>'+ Translator.trans('Description') +': </strong>'+description+'</div>'
    );


    if (anneau_samurai === 'aucun') {
        var terre = $('#terre').text(),
            air = $('#air').text(),
            eau = $('#eau').text(),
            feu = $('#feu').text();
        var select = '<div class="col-md-12">'+
            '<div style="float:right;">'+
            '<label for="quel-anneau">'+ Translator.trans('choose_ring') +'</label>'+
            '<select id="quel-anneau" name="quel-anneau">'+
            '<option value="'+terre+'">'+ Translator.trans('samurai.ring.earth') +'</option>'+
            '<option value="'+air+'">'+ Translator.trans('samurai.ring.air') +'</option>'+
            '<option value="'+eau+'">'+ Translator.trans('samurai.ring.water') +'</option>'+
            '<option value="'+feu+'">'+ Translator.trans('samurai.ring.fire') +'</option>'+
            '</select>'+
            '</div></div>';

        message.append(select);
        var valeur = terre; // Par défaut
        des_lances = parseInt(valeur) + parseInt(rang_samurai);
        jet = des_lances.toString()+'G'+valeur;
    } else {
        des_lances = parseInt(anneau_samurai) + parseInt(rang_samurai);
        jet = des_lances.toString()+'G'+anneau_samurai;
    }

    var button = '<div class="col-md-12"><button style="float:right" class="bouton-preparation-lancer" data-jet="'+jet+'">'+
        Translator.trans('Prepare') +'</button></div>';

    message.append(button);

    $(document).on('change', '#quel-anneau', function(){
        var valeur = $(this).val();
        des_lances = parseInt($('#quel-anneau').val()) + parseInt(rang_samurai);
        var jet = des_lances.toString()+'G'+valeur;
        $('#notes-perso-modal .bouton-preparation-lancer').attr('data-jet', jet);
    });

    $('#notes-perso-modal').pup({
        titre: Translator.trans('spell_details', {'name': nom}),
        hauteur: 600,
        largeur: 500,
        containment: false,
        message: message
    })
}

function majRangs(element) {
    var champ = element.attr('data-champ');
    var rang = $($(element.next('p')).find('>:first-child'));
    var rangPrecVal = parseInt(rang.val());
    if (element.val() === '10') {
        element.val(0);
        if (rangPrecVal < 10) {
            rang.val(rangPrecVal+1);
        }
    } else if(element.val() === '-1') {
        element.val(9);
        var limiteMin =
            champ === 'Honneur' ?
                0 : -10; // La Gloire peut déscendre à -10
        if (rangPrecVal > limiteMin) {
            rang.val(rangPrecVal-1);
        }
    }
    majSamurai(element);
}

function supprimerEquipement(element) {
    var id = element.attr('data-id');
    var nom = element.attr('data-nom');
    if ($('#notes-perso').length > 0) {
        $('#notes-perso-modal').html('');
    }

    var buttons = {};
    buttons[Translator.trans('Validate')]= function() {
        $.ajax({
            url: chemins.supprimerEquipement,
            method: 'post',
            data: {
                id: id,
                nom: nom
            },
            success: function (response) {
                var row = $(element.parents('tr'));
                row.remove();
                toastr["success"](Translator.trans('toast.delete.success', {'name': nom}));
            },
            error: function () {
                toastr["error"](Translator.trans('toast.delete.failure', {'name': nom}));
            }
        });
        $(this).dialog( "close" );
    };
    buttons[Translator.trans('Cancel')]= function() {
        $(this).dialog( "close" );
    };

    $('#notes-perso-modal').pup({
        titre: Translator.trans('pup.title.remove_equipement'),
        message: '<p>'+Translator.trans('pup.text.validate_remove_equipement', {'name': nom})+'</p>',
        boutons: buttons
    })
}

function supprimerArme(element) {
    var id = element.attr('data-id');
    if ($('#notes-perso').length > 0) {
        $('#notes-perso-modal').html('');
    }

    var buttons = {};
    buttons[Translator.trans('Validate')]= function() {
        $.ajax({
            url: chemins.supprimerArme,
            method: 'post',
            data: {id: id},
            success: function (response) {
                $('#arme-' + id).remove();
                toastr["success"](Translator.trans('toast.delete_weapon.success'));
            },
            error: function () {
                toastr["error"](Translator.trans('toast.delete_weapon.failure'));
            }
        });
        $(this).dialog( "close" );
    };
    buttons[Translator.trans('Cancel')]= function() {
        $(this).dialog( "close" );
    };

    $('#notes-perso-modal').pup({
        titre: Translator.trans('pup.title.delete_weapon'),
        message: '<p>'+ Translator.trans('pup.text.validate_delete_weapon') +'</p>',
        boutons: buttons
    })
}

function editerAvantage(element, attribut) {
    var avantage_id = element.attr('data-id');
    var valeur = null;
    if (attribut === 'resume') {
        valeur = element.val();
    } else {
        valeur = element.prop('checked') ? 1 : 0;
    }
    var url = chemins.editerAvantage;
    $.ajax({
        url: url,
        'method': 'post',
        data: {
            avantage_id: avantage_id,
            attribut: attribut,
            valeur: valeur
        },
        success: function (response) {
            if (attribut === 'resume') {
                $('#avantage-perso-' + avantage_id).text(valeur);
            }
            toastr["success"](Translator.trans('toast.update_advantage.success'));
        },
        error: function () {
            toastr["error"](Translator.trans('toast.update_advantage.failure'));
        }
    })
}

function editerDesavantage(element, attribut) {
    var desavantage_id = element.attr('data-id');
    var valeur = null;
    if (attribut === 'resume') {
        valeur = element.val();
    } else {
        valeur = element.prop('checked') ? 1 : 0;
    }
    var url = chemins.editerDesavantage;
    $.ajax({
        url: url,
        'method': 'post',
        data: {
            desavantage_id: desavantage_id,
            attribut: attribut,
            valeur: valeur
        },
        success: function (response) {
            if (attribut === 'resume') {
                $('#desavantage-perso-' + desavantage_id).text(valeur);
            }
            toastr["success"](Translator.trans('toast.update_disadvantage.success'));
        },
        error: function () {
            toastr["error"](Translator.trans('toast.update_disadvantage.failure'));
        }
    })
}

function maj_provisions(element) {
    var provisions = element.val();
    var url = chemins.majProvisions;
    $.ajax({
        url: url,
        method: 'post',
        data: {
            provisions: provisions
        },
        success: function (response) {

        }
    });
}

function maj_notes_joueurs(element) {
    var notes_joueurs = element.val();
    var url = chemins.majNotesJoueurs;
    $.ajax({
        url: url,
        method: 'post',
        data: {
            notes_joueurs: notes_joueurs
        },
        success: function (response) {

        }
    });
}

function initRecuperation() {
    var constitution = parseInt($('#constitution').text());
    var rangReputation = parseInt($('#rang').text());
    var recuperationBasique = constitution * 2 + rangReputation;
    $('#recuperation-basique').text(recuperationBasique);
    var recuperationModificateurs = parseInt($('#recuperation-modificateurs').val());
    $('#recuperation-total').text(recuperationBasique + recuperationModificateurs);
}

function initBlessures() {
    var terre = parseInt($('#terre').text());
    var blessuresPourIndemne = terre * 5;
    $('#indemne').text(blessuresPourIndemne);
    var blessuresParRang = terre * 2;
    $('#rang-de-blessures').text(blessuresParRang);
    var rangs = Custom.definirRangsdeBlessures(terre);
    var blessures = parseInt($('#blessures').val());
    var rangBlessures = getRangBlessures(blessures, rangs);
    $('#rang-actuel').text(rangBlessures.nom);
    $('#malus-actuel').text(rangBlessures.malus);
}

function initNDArmure() {
    var reflexes = parseInt($('#reflexes').text());
    var armureND = parseInt($('#armure-nd').text());
    var basiqueNDArmure = 5 + reflexes * 5 + armureND;
    $('#nd-basique').text(basiqueNDArmure);
    var nDArmureModificateurs = parseInt($('#nd-modificateurs').val());
    $('#nd-total').text(basiqueNDArmure + nDArmureModificateurs);
}

function getDegats(vd, force, filtre) {
    if (filtre === undefined) {
        filtre = '0G0';
    }
    // Si l'arme n'est pas un arc et n'a donc pas de force définie, on récupère la force du samurai
    if (force === 0) {
        force = parseInt($('#force').text());
    }
    var vdArray = Custom.splitG(vd);
    filtre = Custom.splitG(filtre);

    return [(parseInt(vdArray[0]) + force).toString() + 'G' + vdArray[1],
        (parseInt(vdArray[0]) + force + parseInt(filtre[0])).toString() + 'G' +
        (parseInt(vdArray[1]) + parseInt(filtre[1])).toString()];
}

function initInitiative() {
    var initBasique = getJet('rang', 'reflexes')[0];
    $('#init-basique').text(initBasique);
    var initModificateurs = $('#init-modificateurs').val();
    var initTotal = ajouterdBonus(initBasique, initModificateurs);
    $('#init-total').text(initTotal).attr('data-jet', initTotal);
}

function nouvelItem() {

    var nom = $('.new-item').val();
    var equipementBloc = $('#details-et-equipement');

    if (nom !== '') {
        equipementBloc.html('');
        $.ajax({
            url: chemins.nouvelItem,
            method: 'post',
            data: {
                nom: nom
            },
            success: function (response) {
                equipementBloc.html(response.content);
                toastr["success"](Translator.trans('toast.create.success', {'name': nom}));
            },
            error: function () {
                toastr["error"](Translator.trans('toast.create.failure', {'name': nom}));
            }
        })
    }
}

function majMonnaie(monnaie, valeur) {
    $.ajax({
        url: routeMajSamurai,
        type: 'post',
        data: {
            champ: monnaie,
            valeur: valeur
        }
    })
}

function getJet(competence, trait, filtre) {
    if (filtre === undefined) {
        filtre = '0G0';
    }
    var competenceNumber = $('#' + competence);
    competenceNumber = (competenceNumber.length > 0 ) ? parseInt(competenceNumber.text()) : 0;

    var traitString = $('#' + trait).text();
    filtre = Custom.splitG(filtre);
    var traitNumber = parseInt(traitString);


    var desLancesNonFiltre = (competenceNumber + traitNumber).toString();
    var desLances = (competenceNumber + traitNumber + parseInt(filtre[0])).toString();

    return [desLancesNonFiltre + 'G' + traitString, desLances + 'G' + (traitNumber + parseInt(filtre[1])).toString()];
}

function ajouterdBonus(basique, bonus) {
    var basiqueArray = basique.split('G');
    var basiqueLances = parseInt(basiqueArray[0]);
    var basiqueGardes = parseInt(basiqueArray[1]);

    var bonusArray = Custom.splitG(bonus);
    var bonusLances = parseInt(bonusArray[0]);
    var bonusGardes = parseInt(bonusArray[1]);

    var totalLances = basiqueLances + bonusLances;
    var totalGardes = basiqueGardes + bonusGardes;

    return totalLances.toString() + 'G' + totalGardes.toString();
}

function getRangBlessures(blessures, rangs) {
    if (blessures <= rangs[0].marge) {
        return rangs[0];
    } else {
        for(var i = 1; i < rangs.length -1; i++) {
            if (rangs[i - 1].marge < blessures && blessures <= rangs[i].marge) {
                return rangs[i];
            }
        }
        return rangs[8];
    }
}

function afficherTechniquesDuRang(element) {
    var rang = element.attr('data-rang');
    var nom_tech_actuelle = $(element.children('.nom')[0]).text();
    var description_tech_actuelle = $(element.children('.description')[0]).text();
    var id_tech_actuelle = element.attr('data-id');
    var voie_tech_actuelle = element.attr('data-voie');
    var requiert_tech_actuelle = element.attr('data-requiert');

    //var validate = ;
    var buttons = {};
    var validate_txt = Translator.trans('pup.button.swap_technique');
    buttons[validate_txt]= function() {
        var technique_selectionnee = $('.technique_a_choisir.active');
        var nouvelle_technique_id = technique_selectionnee.attr('data-id');
        // Pas la peine d'importuner le serveur s'il n'y a pas besoin.
        if (id_tech_actuelle === nouvelle_technique_id) {
            $(this).dialog( "close" );
        };
        $.ajax({
            url: chemins.majSamuraiTechnique,
            method: 'post',
            data: {
                rank: rang,
                technique_id: nouvelle_technique_id
            },
            success: function (response) {
                toastr["success"](Translator.trans('toast.swap_techniques.success'));
                // La nouvelle technique remplace l'ancienne
                var tech_sel_name = $(technique_selectionnee.find('.nom')[0]).text();
                var tech_sel_description = $(technique_selectionnee.children('p')[0]).text();
                var nouvelle_technique_voie = $(technique_selectionnee.find('.voie')[0]).text();
                var nouvelle_technique_requiert = $(technique_selectionnee.find('.requiert')[0]).text();
                $(element.children('.nom')[0]).text(tech_sel_name);
                $(element.children('.description')[0]).text(tech_sel_description);
                element.attr('data-id', nouvelle_technique_id);
                element.attr('data-voie', nouvelle_technique_voie);
                element.attr('data-requiert', nouvelle_technique_requiert);

                // L'ancienne technique remplace la nouvelle (cachée)
                $('.techniques-alternatives.technique-'+ nouvelle_technique_id)
                    .removeClass('technique-'+ nouvelle_technique_id)
                    .addClass('technique-'+ id_tech_actuelle)
                    .attr({
                        'data-id': id_tech_actuelle,
                        'data-tech-alt-nom': nom_tech_actuelle,
                        'data-voie': voie_tech_actuelle,
                        'data-requiert': requiert_tech_actuelle
                    })
                    .text(description_tech_actuelle);
            },
            error: function () {
                toastr["error"](Translator.trans('toast.swap_techniques.failure'));
            }
        });
        $(this).dialog( "close" );
    };
    buttons[Translator.trans('Annuler')] = function() {
        $(this).dialog( "close" );
    };
    var contenu = $('<div/>').attr('id', 'techniques-rang-' + rang);

    // Pouvoir choisir la technique actuelle
    var nom_tech_actuelle = $(element.children('.nom')[0]).text();
    var description_tech_actuelle = $(element.children('.description')[0]).text();
    var id_tech_actuelle = element.attr('data-id');
    var voie_actuelle = element.attr('data-voie');
    var requiert_actuelle = element.attr('data-requiert');
    var bloc_tech_actuelle = $('<div/>').attr({
        'class': 'col-md-12 technique_a_choisir active',
        'data-id': id_tech_actuelle
    });
    bloc_tech_actuelle.html(
        '<div class="col-md-12 center"><strong class="voie">'+voie_actuelle+'</strong></div>'+
        '<div class="col-md-12">'+
        '<div class="col-md-6"><strong class="nom">'+nom_tech_actuelle+'</strong></div>' +
        '<div class="col-md-6 requiert" style="text-align: right">'+requiert_actuelle+'</div>'+
        '</div>'+
        '<p>'+description_tech_actuelle	+'</p>'
    );
    $(contenu).append(bloc_tech_actuelle);

    // Afficher toutes les techniques disponibles
    $(element.children('.techniques-alternatives')).each(function() {
        var tech_alt = $(this);
        var nom = tech_alt.attr('data-tech-alt-nom');
        var technique_description = tech_alt.text();
        var technique_id = tech_alt.attr('data-id');
        var technique_voie = tech_alt.attr('data-voie');
        var technique_requiert = tech_alt.attr('data-requiert');
        var bloc_technique = $('<div/>').attr({
            'class': 'col-md-12 technique_a_choisir',
            'data-id': technique_id
        });
        bloc_technique.html(
            '<div class="col-md-12 center"><strong class="voie">'+technique_voie+'</strong></div>'+
            '<div class="col-md-12">'+
            '<div class="col-md-6"><strong class="nom">'+nom+'</strong></div>' +
            '<div class="col-md-6 requiert" style="text-align: right">'+technique_requiert+'</div>'+
            '</div>'+
            '<p>'+technique_description	+'</p>'
        );
        $(contenu).append(bloc_technique);
    });
    // var btn_save = $('<button/>').attr({
    // 'class': 'btn save changer-technique'
    // }).html('Changer');
    // $(contenu).append(btn_save);
    console.log(buttons);
    $('#notes-perso-modal').pup({
        titre: 'Techniques du rang ' + rang,
        modal: true,
        resizable: true,
        position: {
            my: 'center center'
        },
        containment: false,
        hauteur: 600,
        largeur: 1000,
        boutons: buttons,
        message: contenu
    });
}

function supprimerPersonnage(element) {
    var id = element.attr('data-personnage-id');
    var nom = element.attr('data-personnage-nom');
    if ($('#notes-perso').length > 0) {
        $('#notes-perso-modal').html('');
    }

    var buttons = {};
    buttons[Translator.trans('Validate')] = function() {
        $.ajax({
            url: chemins.supprimerPersonnage,
            method: 'post',
            data: {
                id: id,
                nom: nom
            },
            success: function (response) {
                var row = $(element.parents('tr'));
                row.remove();
                toastr["success"](Translator.trans('toast.delete.success', {'name': nom}));
            },
            error: function () {
                toastr["error"](Translator.trans('toast.delete.failure', {'name': nom}));
            }
        });
        $(this).dialog( "close" );
    };
    buttons[Translator.trans('Cancel')]= function() {
        $(this).dialog( "close" );
    };

    $('#notes-perso-modal').pup({
        titre: Translator.trans('pup.title.remove_npc'),
        message: '<p>'+ Translator.trans('pup.text.validate_remove_npc', {'name': nom}) +'</p>',
        boutons: buttons
    })
}

/**
 * Permet de changer la vue avantages-techniques
 * */
function toggleVues(element) {
    var voir_detaillee = element.prop('checked');
    var id = '#' + element.attr('data-id');

    if(voir_detaillee) {
        $(id + ' .vue-simplifiee').hide('fold', function() {
            $(id + ' .vue-detaillee').show('fold');
        });
    } else {
        $(id + ' .vue-detaillee').hide('fold', function() {
            $(id + ' .vue-simplifiee').show('fold');
        });
    }
}