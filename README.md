L5A Dashboard
========================

Welcome to my custom dashboard for the paper role playing game 
Legend of the Five Rings.

I developed this dashboard because though role playing games are best played
face to face, sometimes the only way to play is online through some chat. This experience is 
too often the cause for players to stop gathering and live adventures together.

But, this experience doesn't need to be a bad one. Instead of using the complicated and unfriendly 
csv files or Google Sheets, I wanted for my players and friends to have a useful user-friendly interface 
to keep track of their character and ultimately of what is going on around them.

What's inside?
--------------

With the L5A Dashboard, players can

  * Manage their character and compare it to the others;

  * Have any information they may need one click away;

  * Prepare pre-calculated (but still customisable) dice rolls before throwing them;

  * Browse through a 'shop' for techniques, weapons, skills, spells, attribute points and other stuff to buy
   for their character;

  * Write personal notes to keep track of what is going on and on future plans!;
  
  * The possibility to customise weapons and rolls;

  * Access to a GameMaster dashboard to create non-player characters, new items and give them to their players.
  
  * Connect to their own custom game as a player or create new games as gamemaster
  
  * Combat page managed by the gamemaster but sent to the players so that they can keep an eye on what's going on
  
  * Map with a travel-saving feature. (users can save coordonates, drawing a journey in the process)


TODOs:
------

  * Make sure everything works - Every. Single. Page. And. Feature.
  * Change skill logic to implement a samuraiXrefCompetence table with fields like:
    * id
    * samurai_id
    * skill_id
    * rank
    * specialities
  * Use Repositories and rework Services to make the code cleaner and the site faster.
  *  chat implemented with Pusher


  For now I left the SEO aspect of website management aside as in first step I am developing this dashboard for my friends and focusing on adding features.
  Once I implement the accounts, then I want to put it at disposal of any who would have a use for it.
  In French and mostly in English.

  The result so far : http://symfony-yivh.frb.io/en/index