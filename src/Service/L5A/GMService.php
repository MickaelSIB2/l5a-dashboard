<?php
/**
 * Created by PhpStorm.
 * User: Mickaël
 * Date: 24/06/2019
 * Time: 11:45
 */

namespace App\Service\L5A;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\TranslatorInterface;
use App\Entity\L5A\Personnage;
use App\Entity\L5A\Competence;
use App\Entity\L5A\Arme;
use App\Entity\L5A\Armure;
use App\Entity\L5A\Avantage;
use App\Entity\L5A\Desavantage;
use App\Entity\L5A\Technique;
use App\Entity\L5A\PnjBonus;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use App\Form\L5A\PersonnageType;
use App\Form\L5A\ArmeType;
use App\Form\L5A\ArmureType;
use App\Form\L5A\AvantageType;
use App\Form\L5A\DesavantageType;
use App\Form\L5A\TechniqueType;

class GMService {
  private $em;
  private $gameId;
  private $translator;
  private $urlGenerator;
  private $formFactory;

  /**
   * GMService constructor.
   */
  public function __construct(
    TranslatorInterface $translator,
    UrlGeneratorInterface $urlGenerator,
    FormFactoryInterface $formFactory
  ) {
    $this->translator = $this->translator;
    $this->urlGenerator = $urlGenerator;
    $this->formFactory = $formFactory;
  }

  public function setEM(EntityManager $em) : self {
    $this->em = $em;
    return $this;
  }

  public function setGameId(int $gameId) : self {
    $this->gameId = $gameId;
    return $this;
  }

  public function getGameId() : int {
    return $this->gameId;
  }

  /**
  * @var string $type should be 'pnj'
  **/
  public function getNPCForm (string $type = '', $id = null) {
    // Si un type et un id sont définis, éditer le type (pnj, arme, armure...)
    // PNJ Form
    if ($this->em === null) {
      throw new \Exception("No EntityManager defined", 1);
    };
      $perso = null;
      $action = null;
    if ($type !== '' && $type === 'pnj' && $id !== null) {
      $perso = $this->em->getRepository(Personnage::class)->findOneBy(array('id' => $id));
      if ($perso->getGameId() !== (int)$this->gameId) {
        throw $this->createAccessDeniedException(
          $this->translator->trans('gm.unauthorized', array('%type%' => 'ce personnage'))
        );
      }
      $action = $this->urlGenerator->generate('gm_editer_pnj');

    } else {
      $perso = new Personnage();
      $action = $this->urlGenerator->generate('gm_creation_pnj');
    }

    return $this->formFactory->create(PersonnageType::class, $perso, array(
      'action' => $action,
    ));
    // Fin PNJ Form
  }

  public function getWeaponForm($type, $id = null) {
    // Arme Form
    if ($this->em === null) {
      throw new \Exception("No EntityManager defined", 1);
    };
    $arme = null;
    $action = null;
    if ($type !== '' && $type === 'arme') {
      $arme = $this->em->getRepository(Arme::class)->findOneBy(array('id' => $id));
      if ($arme->getGameId() !== $this->gameId) {
        throw $this->createAccessDeniedException(
          $this->translator->trans('gm.unauthorized', array('%type%' => 'cette arme'))
        );
      }
      $action = $this->urlGenerator->generate('gm_editer_arme');
    } else {
      $arme = new Arme();
      $action = $this->urlGenerator->generate('gm_creation_arme');
    }

    return $this->formFactory->create(ArmeType::class, $arme, array(
      'action' => $action,
    ));
    // Fin Arme Form
  }

  public function getArmorForm($type, $id = null) {
    if ($this->em === null) {
      throw new \Exception("No EntityManager defined", 1);
    };
    // Armor form
    $armure = null;
    $action = null;
    if ($type !== '' && $type === 'armure') {
      $armure = $this->em->getRepository(Armure::class)->findOneBy(array('id' => $id));
      if ($armure->getGameId() !== $this->gameId) {
        throw $this->createAccessDeniedException(
          $translator->trans('gm.unauthorized', array('%type%' => 'cette armure'))
        );
      }
      $action = $this->urlGenerator->generate('gm_editer_armure');
    } else {
      $armure = new Armure();
      $action = $this->urlGenerator->generate('gm_creation_armure');
    }

    return $this->formFactory->create(ArmureType::class, $armure, array(
      'action' => $action,
    ));
    // Fin Armure Form
  }

  public function getAdvantageForm($type, $id) {
    // Avantage Form
    if ($this->em === null) {
      throw new \Exception("No EntityManager defined", 1);
    };
      $avantage = null;
      $action = null;
      if ($type !== '' && $type === 'avantage') {
        $avantage = $this->em
        ->getRepository(Avantage::class)->findOneBy(array('id' => $id));
        if ($avantage->getGameId() !== $this->gameId) {
          throw $this->createAccessDeniedException(
            $translator->trans('gm.unauthorized', array('%type%' => 'cet avantage'))
          );
        }
        $action = $this->urlGenerator->generate('gm_editer_avantage');
      } else {
        $avantage = new Avantage();
        $action = $this->urlGenerator->generate('gm_creation_avantage');
      }

      return $this->formFactory->
      create(AvantageType::class, $avantage, array(
        'action' => $action,
      ));
      // Fin Avantage Form
  }

  public function getDisadvantageForm($type, $id) {
    // Désavantage Form
    if ($this->em === null) {
      throw new \Exception("No EntityManager defined", 1);
    };
    $avantage = null;
    $action = null;
    if ($type !== '' && $type === 'desavantage') {
      $avantage = $this->em
      ->getRepository(Desavantage::class)->findOneBy(array('id' => $id));
      if ($avantage->getGameId() !== $this->gameId) {
        throw $this->createAccessDeniedException(
          $translator->trans('gm.unauthorized', array('%type%' => 'ce désavantage'))
        );
      }
      $action = $this->urlGenerator->generate('gm_editer_desavantage');
    } else {
      $avantage = new Desavantage();
      $action = $this->urlGenerator->generate('gm_creation_desavantage');
    }

    return $this->formFactory->
    create(DesavantageType::class, $avantage, array(
      'action' => $action,
    ));
    // Fin Désavantage Form
  }

  public function getTechniqueForm($type, $id) {
    // Désavantage Form
    if ($this->em === null) {
      throw new \Exception("No EntityManager defined", 1);
    };
    $avantage = null;
    $action = null;
    if ($type !== '' && $type === 'technique') {
      $avantage = $this->em
      ->getRepository(Technique::class)->findOneBy(array('id' => $id));
      if ($avantage->getGameId() !== $this->gameId) {
        throw $this->createAccessDeniedException(
          $translator->trans('gm.unauthorized', array('%type%' => 'cette technique'))
        );
      }
      $action = $this->urlGenerator->generate('gm_editer_technique');
    } else {
      $avantage = new Technique();
      $action = $this->urlGenerator->generate('gm_creation_technique');
    }

    return $this->formFactory->
    create(TechniqueType::class, $avantage, array(
      'action' => $action,
    ));
    // Fin Désavantage Form
  }

// Samurais
  public function getGameSamurai(int $gameId) : array {
    if ($this->em === null) {
      throw new \Exception("No EntityManager defined", 1);
    };
    if ($this->gameId === null) {
      throw new \Exception("No GameId defined", 1);
    };

    $query = $this->em->createQuery(
      'SELECT s.id, s.nom, s.xp, s.blessures, s.honneur, s.gloire, s.statut,
      s.souillure, s.reputation, s.joueur
      FROM App\Entity\L5A\Samurai s
      INDEX BY s.id
      WHERE s.gameId = ' .$this->gameId.''
    );
    return
      sizeof($query->getResult()) == 0 ?
        null:
        $query->getResult();
  }

// Joueurs
  public function getGamePlayers(int $gameId) : array {
    if ($this->em === null) {
      throw new \Exception("No EntityManager defined", 1);
    };
    if ($this->gameId === null) {
      throw new \Exception("No GameId defined", 1);
    };

    $query = $this->em->createQuery(
      'SELECT u.id, u.username
    FROM App\Entity\L5A\AppUser u, App\Entity\L5A\PartieXJoueur pxj
    INDEX BY u.id
    WHERE pxj.gameId = ' .$this->gameId.'
    AND pxj.joueurId = u.id'
    );
    return
      sizeof($query->getResult()) == 0 ?
        null:
        $query->getResult();
  }

  // Set bonus to NPC
  /**
   * @param $data // All fields from
   * @param $pnjIdrequest
   * @param $flush
   */
    public function setBonus($data, int $pnjId, bool $flush = false) {
      if ($this->em === null) {
        throw new \Exception("No EntityManager defined", 1);
      };
      $aBonus = array();
      foreach($data as $key => $value) {
        if (strpos($key, 'bonus') !== false) {
          // rassembler les bonus entre eux
          $aField = explode('_', $key);
          $nb = $aField[2];
          $type = $aField[1];
          $aBonus[$nb][$type] = $value;
        }
      }

      if (! empty($aBonus)) {
        // Ajouter tous les bonus au pnj
        foreach ($aBonus as $bonus) {
          if (isset($bonus['Nom'])) {
            $newBonus = new PnjBonus();
            $newBonus->setIdPnj($pnjId);
            $newBonus->setType($bonus['Type']);
            $newBonus->setNom($bonus['Nom']);
            $newBonus->setDescription($bonus['Description']);
            $this->em->persist($newBonus);
          }
        }
        if ($flush)
          $this->em->flush();
      }
    }

    public function newSkill($form, $flush = false) : Competence {
      try {
        $newCompetence = $form->getData();
        $this->em->persist($newCompetence);
      } catch (Exception $e) {
        throw new \Exception('Couldn\t create new skill : ' . $e, 1);
      }
      if ($flush)
        $this->em->flush();
      return $newCompetence;
    }

    public function newNPC($form, $flush = false) : Personnage {
      try {
        /** @var Personnage $newPNJ */
        $newPNJ = $form->getData();
        $newPNJ->setGenereGm(1);
        $newPNJ->setGameId($this->gameId);
        $this->em->persist($newPNJ);
      } catch (Exception $e) {
        throw new \Exception('Couldn\t create new npc : ' . $e, 1);
      }
      if ($flush)
        $this->em->flush();

      return $newPNJ;
    }
}
