<?php
/**
 * Created by PhpStorm.
 * User: micka
 * Date: 17/09/2018
 * Time: 08:32
 */

namespace App\Service;

use App\Entity\L5A\SamuraiXrefCharacter;
use App\Entity\L5A\SamuraiXrefNotes;
use App\Entity\L5A\SamuraiXrefPlace;
use App\Entity\L5A\Samurai;
use App\Entity\L5A\SamuraiXrefArrow;
use App\Entity\L5A\SamuraiXrefSkill;
use App\Entity\L5A\SamuraiXrefSpell;
use App\Entity\L5A\SamuraiXrefTechnique;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class DatabaseUpdater
 * @package App\Service
 * This class should only be used to save data from
 * tables in a new format as the pages are loaded,
 * for instance, the skills from the samurai table
 * will be stored in a new table instead.
 */
class DatabaseUpdater {

	static function saveCompetences(?EntityManager $em, ?Samurai $samurai) {
		$string = $samurai->getCompetences();
		$competences = explode(';', $string);
		foreach ($competences as $competence) {
			if (!is_null($competence)) {
				$competenceDetails = explode(',', $competence);
				$samuraiXSkills = new SamuraiXrefSkill();
				//showme($competenceDetails[1]);
				$samuraiXSkills
					->setSamuraiId($samurai->getId())
					->setSkillId((int)$competenceDetails[0])
					->setRank((int)$competenceDetails[1])
					->setSpecialities(isset($competenceDetails[2]) ? $competenceDetails[2] : '');
				try {
					$em->persist($samuraiXSkills);
				} catch (ORMException $e) {
					die($e);
				}
			}
		}
		$samurai->setCompetences('');
		DatabaseUpdater::mergeNFlush($em, $samurai);
	}

	static function saveSpells(?EntityManager $em, ?Samurai $samurai, ?string $shugenjaType) {
		if ($samurai->getType() !== $shugenjaType) {
			return;
		}

		$spells = $samurai->getSorts();
		foreach ($spells as $oneSpell) {
			if (!is_null($oneSpell)) {
				$samuraiXSpell = new SamuraiXrefSpell();
				$samuraiXSpell
					->setSamuraiId($samurai->getId())
					->setSpellId((int)$oneSpell);
				try {
					$em->persist($samuraiXSpell);
				} catch (ORMException $e) {
					die($e);
				}
			}
		}
		$samurai->setSorts(array(), true);
		DatabaseUpdater::mergeNFlush($em, $samurai);
	}

	static function saveCharacters(?EntityManager $em, ?SamuraiXrefNotes $notesCampagne, ?int $samuraiId) {

		$characters = explode(',', $notesCampagne->getPersonnagesRencontres());
		foreach ($characters as $oneCharacter) {
			if (!is_null($oneCharacter)) {
				$notesXCharacter = new SamuraiXrefCharacter();
				$notesXCharacter
					->setSamuraiId($samuraiId)
					->setCharacterId((int)$oneCharacter);
				try {
					$em->persist($notesXCharacter);
				} catch (ORMException $e) {
					die($e);
				}
			}
		}
		$notesCampagne->setPersonnagesRencontres('');
		DatabaseUpdater::mergeNFlush($em, $notesCampagne);
	}

	static function savePlaces(?EntityManager $em, ?SamuraiXrefNotes $notesCampagne, ?int $samuraiId) {

		$places = explode(',', $notesCampagne->getEndroitsVisites());
		foreach ($places as $onePlace) {
			if (!is_null($onePlace)) {
				$notesXPlace = new SamuraiXrefPlace();
				$notesXPlace
					->setSamuraiId($samuraiId)
					->setPlaceId((int)$onePlace);
				try {
					$em->persist($notesXPlace);
				} catch (ORMException $e) {
					die($e);
				}
			}
		}
		$notesCampagne->setEndroitsVisites('');
		DatabaseUpdater::mergeNFlush($em, $notesCampagne);
	}

	static function saveArrows(?EntityManager $em, Samurai $samurai) {

		$arrows = explode(';', $samurai->getFlechesRestantes());
		foreach ($arrows as $oneArrow) {
			if (!is_null($oneArrow)) {
				$arrowDetails = explode(',', $oneArrow);
				$samuraiXArrow = new SamuraiXrefArrow();
				$samuraiXArrow
					->setSamuraiId($samurai->getId())
					->setArrowId((int)$arrowDetails[0])
					->setQuantity((int)$arrowDetails[1]);
				try {
					$em->persist($samuraiXArrow);
				} catch (ORMException $e) {
					die($e);
				}
			}
		}
		$samurai->setFlechesRestantes('');
		DatabaseUpdater::mergeNFlush($em, $samurai);
	}

	static function saveTechnique(?EntityManager $em, Samurai $samurai, ?int $techniqueRank, ?int $techniqueId) {
		$samuraiXTechnique = new SamuraiXrefTechnique();
		$samuraiXTechnique
			->setSamuraiId($samurai->getId())
			->setRank($techniqueRank)
			->setTechniqueId($techniqueId);

		try {
			$em->persist($samuraiXTechnique);
		} catch (ORMException $e) {
			die($e);
		}
		$function = 'setTechnique'.$techniqueRank;
		$samurai->$function(null);

		DatabaseUpdater::mergeNFlush($em, $samurai);
	}

	static function mergeNFlush(?EntityManager $em, $object) {
		try {
			$em->merge($object);
		} catch (ORMException $e) {
			die($e);
		}
		try {
			$em->flush();
		} catch (OptimisticLockException $e) {
			die($e);
		} catch (ORMException $e) {
			die($e);
		}
	}
}