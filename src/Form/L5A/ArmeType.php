<?php

namespace App\Form\L5A;

use App\Entity\L5A\Arme;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ArmeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('id', HiddenType::class)
			->add('nom', TextType::class, array(
				'required' => true,
				'label' => 'Nom',
				'translation_domain' => 'messages'
			))
			->add('type', ChoiceType::class, array(
				'required' => false,
				'choices' => array(
					'arme.arme_lourde',
					'arme.arc',
					'arme.hast',
					'arme.chain',
					'arme.staff',
					'arme.knife',
					'arme.spear',
					'arme.ninjutsu',
					'arme.sword',
					'arme.fan'
				),
				'choice_label' => function ($value, $key, $index) {
					return $value;
				},
				'placeholder' => 'form.choose_type',
				'choice_translation_domain' => 'messages',
				'translation_domain' => 'messages'
			))
			->add('motsCles', TextType::class, array(
				'required' => false,
				'label' => 'keyword',
				'translation_domain' => 'messages'
			))
			->add('vd', TextType::class, array(
				'required' => false,
				'label' => 'arme.vd',
				'translation_domain' => 'messages'
			))
			->add('force', TextType::class, array(
				'required' => false,
				'label' => 'arme.force',
				'translation_domain' => 'messages'
			))
			->add('portee')
			->add('prix', NumberType::class, array(
				'required' => false,
				'label' => 'price_zeni',
				'translation_domain' => 'messages'
			))
			->add('note', TextAreaType::class, array(
				'required' => false,
				'label' => 'arme.note',
				'translation_domain' => 'messages'
			))
			->add('description')    
			->add('save', SubmitType::class, array(
				'attr' => array('class' => 'save btn'),
				'label' => 'form.save',
				'translation_domain' => 'messages'					
			))
			->add('annuler', ButtonType::class, array(
				'attr' => array(
					'class' => 'annuler btn col-md-offset-1 editer-gm',
					'data-id' => 'new',
					'data-edit' => 'Arme'
				),
				'label' => 'form.cancel',
				'translation_domain' => 'messages'	
			))
			;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Arme::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'l5a_arme';
    }


}
