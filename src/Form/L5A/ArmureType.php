<?php

namespace App\Form\L5A;

use App\Entity\L5A\Armure;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
// use Doctrine\DBAL\Types\IntegerType;
// use Doctrine\ORM\EntityRepository;
// use Doctrine\ORM\Mapping\Entity;
// use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


class ArmureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('id', HiddenType::class)
			->add('nom', TextType::class, array(
				'required' => true,
				'label' => 'Nom',
				'translation_domain' => 'messages'
			))
			->add('nd', NumberType::class, array(
				'required' => true,
				'label' => 'armure.nd',
				'translation_domain' => 'messages'
			))
			->add('reduction', NumberType::class, array(
				'required' => true,
				'label' => 'armure.reduction',
				'translation_domain' => 'messages'
			))
			->add('reglesSpeciales', TextType::class, array(
				'required' => true,
				'label' => 'armure.special_rules',
				'translation_domain' => 'messages'
			))
			->add('description')
			->add('prix', NumberType::class, array(
				'required' => false,
				'label' => 'price_zeni',
				'translation_domain' => 'messages'
			))  
			->add('save', SubmitType::class, array(
				'attr' => array('class' => 'save btn'),
				'label' => 'form.save',
				'translation_domain' => 'messages'		
			))
	        ->add('annuler', ButtonType::class, array(
		        'attr' => array(
			        'class' => 'annuler btn col-md-offset-1 editer-gm',
			        'data-id' => 'new',
			        'data-edit' => 'Armure'
		        ),
        		'label' => 'form.cancel',
        		'translation_domain' => 'messages'	
	        ))
		;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Armure::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'l5a_armure';
    }


}
