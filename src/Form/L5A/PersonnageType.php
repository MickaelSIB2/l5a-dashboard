<?php

namespace App\Form\L5A;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;
use App\Entity\L5A\Personnage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Button;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//use Symfony\Component\Form\Extension\Core\Type\FloatType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class PersonnageType extends AbstractType {
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		/** Samurai $samurai */
		$samurai = isset($options['data']) && $options['data']->getId() !== null ?
			$options['data'] : null;
		$builder
			->add('id', HiddenType::class)
			->add('nom', TextType::class, array(
				'required' => true,
				'label' => 'Nom',
				'translation_domain' => 'messages'
			))
			->add('rapport', TextType::class, array(
				'required' => true,
				'label' => 'character.relationship',
				'translation_domain' => 'messages'
			))
			->add('clan')
			->add('honneur', IntegerType::class, array(
				'required' => false,
				'label' => 'samurai.honour',
				'translation_domain' => 'messages'
			))
			->add('rangMaitrise', IntegerType::class, array(
				'required' => false,
				'label' => 'samurai.rank',
				'translation_domain' => 'messages'
			))
			->add('notes')
			->add('air', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getAir() : 2
			))
			->add('reflexes', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getReflexes() : 2
			))
			->add('intuition', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getIntuition() : 2
			))
			->add('eau', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getEau() : 2,
				'label' => 'samurai.ring.water',
				'translation_domain' => 'messages'
			))
			->add('force', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getForce() : 2,
				'label' => 'samurai.trait.strength',
				'translation_domain' => 'messages'
			))
			->add('perception', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getPerception() : 2
			))
			->add('terre', IntegerType::class, array(
				'required' => true,
				'data' => $samurai !== null ? $samurai->getTerre() : 2,
				'label' => 'samurai.ring.earth',
				'translation_domain' => 'messages'
			))
			->add('constitution', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getConstitution() : 2
			))
			->add('volonte', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getVolonte() : 2,
				'label' => 'samurai.trait.will',
				'translation_domain' => 'messages'
			))
			->add('feu', IntegerType::class, array(
				'required' => true,
				'data' => $samurai !== null ? $samurai->getFeu() : 2,
				'label' => 'samurai.ring.fire',
				'translation_domain' => 'messages'
			))
			->add('agilite', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getAgilite() : 2,
				'label' => 'samurai.trait.agility',
				'translation_domain' => 'messages'
			))
			->add('intelligence', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getIntelligence() : 2
			))
			->add('vide', IntegerType::class, array(
				'required' => false,
				'data' => $samurai !== null ? $samurai->getVide() : 2,
				'label' => 'samurai.ring.void',
				'translation_domain' => 'messages'
			))
			->add('rangBlessure1', IntegerType::class, array(
				'label' => 'wounds_rank.indemn',
				'required' => false,
				'translation_domain' => 'messages'
			))
			->add('rangBlessure2', IntegerType::class, array(
				'label' => 'wounds_rank.scratched',
				'required' => false
			))
			->add('rangBlessure3', IntegerType::class, array(
				'label' => 'wounds_rank.slightly_wounded',
				'required' => false,
				'translation_domain' => 'messages'
			))
			->add('rangBlessure4', IntegerType::class, array(
				'label' => 'wounds_rank.wounded',
				'required' => false,
				'translation_domain' => 'messages'
			))
			->add('rangBlessure5', IntegerType::class, array(
				'label' => 'wounds_rank.gravely_wounded',
				'required' => false,
				'translation_domain' => 'messages'
			))
			->add('rangBlessure6', IntegerType::class, array(
				'label' => 'wounds_rank.disabled',
				'required' => false,
				'translation_domain' => 'messages'
			))
			->add('rangBlessure7', IntegerType::class, array(
				'label' => 'wounds_rank.exhausted',
				'required' => false,
				'translation_domain' => 'messages'
			))
			->add('rangBlessure8', IntegerType::class, array(
				'label' => 'wounds_rank.out',
				'required' => true,
				'translation_domain' => 'messages'
			))
			->add('nd', IntegerType::class, array(
				'required' => false,
				'label' => 'armure.nd',
				'translation_domain' => 'messages'
			))
			->add('reduction', IntegerType::class, array(
				'required' => false,
				'label' => 'armure.reduction',
				'translation_domain' => 'messages'
			))
			->add('jetAttaque', TextType::class, array(
				'required' => false,
				'label' => 'attack_roll',
				'translation_domain' => 'messages'
			))
			->add('jetDegats', TextType::class, array(
				'required' => false,
				'label' => 'damage_roll',
				'translation_domain' => 'messages'
			))
			->add('jetInitiative', TextType::class, array(
				'required' => false,
				'label' => 'init_roll',
				'translation_domain' => 'messages'
			))
			->add('arme', TextType::class, array(
				'required' => false,
				'label' => 'weapon',
				'translation_domain' => 'messages'
			))
			->add('armure', TextType::class, array(
				'required' => false,
				'label' => 'armor',
				'translation_domain' => 'messages'
			))
//			->add('techniquesSorts', TextareaType::class, array(
//				'required' => false,
//				'label' => 'Techniques, sorts et autres bonus',
//				'attr' => array('rows' => 4)
//			))
			->add('save', SubmitType::class, array(
					'attr' => array(
						'class' => 'save btn',
						'data-id' => 'new',
						'data-edit' => 'PNJ'
					),
					'label' => 'form.save',
					'translation_domain' => 'messages'	
				)
			)
			->add('annuler', ButtonType::class, array(
				'attr' => array(
					'class' => 'annuler btn col-md-offset-1 editer-gm',
					'data-id' => 'new',
					'data-edit' => 'PNJ'
				),
				'label' => 'form.cancel',
				'translation_domain' => 'messages'
			))
		;
		/*->add('linkedTo', EntityType::class, array(
				'class' => Personnage::class,
				'choice_label' => 'nom',
				'choice_value' => 'id',
				'required' => false,
				'multiple' => true,
 				'label' => 'Lié à',
				'query_builder' => function (EntityRepository $er) {
			        return $er->createQueryBuilder('p')
				        ->where('p.genereGm = 1')
			            ->orderBy('p.nom', 'ASC');
                },				
			))*/
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(
			array(
				'data_class' => Personnage::class,
				'attr' => array(
					'class' => '',
				),
				'allow_extra_fields' => true,
				'csrf_protection' => false,
				'validation_groups' => false,
			)
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'l5a_personnage';
	}
}
