<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\L5A\SamuraiXrefTechniqueRepository")
 */
class SamuraiXrefTechnique
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $samuraiId;

    /**
     * @ORM\Column(type="integer")
     */
    private $techniqueId;

    /**
     * @ORM\Column(type="integer")
     */
    private $rank;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSamuraiId(): ?int
    {
        return $this->samuraiId;
    }

    public function setSamuraiId(int $samuraiId): self
    {
        $this->samuraiId = $samuraiId;

        return $this;
    }

    public function getTechniqueId(): ?int
    {
        return $this->techniqueId;
    }

    public function setTechniqueId(int $techniqueId): self
    {
        $this->techniqueId = $techniqueId;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }
}
