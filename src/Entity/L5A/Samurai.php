<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Samurai
 *
 * @ORM\Table(name="samurai")
 * @ORM\Entity(repositoryClass="App\Repository\L5A\SamuraiRepository")
 */
class Samurai
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="clan_id", type="integer", nullable=true)
     */
    private $clanId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="clan_name", type="string", length=50, nullable=true)
     */
    private $clanName;

    /**
     * @var int|null
     *
     * @ORM\Column(name="famille_id", type="integer", nullable=true)
     */
    private $familleId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="famille_name", type="string", length=50, nullable=true)
     */
    private $familleName;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ecole_id", type="integer", nullable=true)
     */
    private $ecoleId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ecole_name", type="string", length=50, nullable=true)
     */
    private $ecoleName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="armes", type="string", length=50, nullable=true)
     */
    private $armes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="equipement", type="string", length=250, nullable=true)
     */
    private $equipement;

    /**
     * @var int|null
     *
     * @ORM\Column(name="armure", type="integer", nullable=true)
     */
    private $armure;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false, options={"default"="Bushi"})
     */
    private $type = 'Bushi';

    /**
     * @var int
     *
     * @ORM\Column(name="xp", type="integer", nullable=false)
     */
    private $xp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="xp_restant", type="integer", nullable=true)
     */
    private $xpRestant;

    /**
     * @var string|null
     *
     * @ORM\Column(name="joueur", type="string", length=255, nullable=true)
     */
    private $joueur;

    /**
     * @var int
     *
     * @ORM\Column(name="rang", type="integer", nullable=false, options={"default"="1"})
     */
    private $rang = '1';

    /**
     * @var int|null
     *
     * @ORM\Column(name="reputation", type="integer", nullable=true, options={"default"="1"})
     */
    private $reputation = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="competences", type="text", length=65535, nullable=true)
     */
    private $competences;

    /**
     * @var string|null
     *
     * @ORM\Column(name="katas", type="string", length=50, nullable=true)
     */
    private $katas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="avantages", type="string", length=250, nullable=true)
     */
    private $avantages;

    /**
     * @var string|null
     *
     * @ORM\Column(name="desavantages", type="string", length=250, nullable=true)
     */
    private $desavantages;

    /**
     * @var int
     *
     * @ORM\Column(name="terre", type="integer", nullable=false, options={"default"="2"})
     */
    private $terre = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="constitution", type="integer", nullable=false, options={"default"="2"})
     */
    private $constitution = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="volonte", type="integer", nullable=false, options={"default"="2"})
     */
    private $volonte = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="air", type="integer", nullable=false, options={"default"="2"})
     */
    private $air = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="reflexes", type="integer", nullable=false, options={"default"="2"})
     */
    private $reflexes = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="intuition", type="integer", nullable=false, options={"default"="2"})
     */
    private $intuition = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="eau", type="integer", nullable=false, options={"default"="2"})
     */
    private $eau = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="_force", type="integer", nullable=false, options={"default"="2"})
     */
    private $force = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="perception", type="integer", nullable=false, options={"default"="2"})
     */
    private $perception = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="feu", type="integer", nullable=false, options={"default"="2"})
     */
    private $feu = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="agilite", type="integer", nullable=false, options={"default"="2"})
     */
    private $agilite = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="intelligence", type="integer", nullable=false, options={"default"="2"})
     */
    private $intelligence = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="vide", type="integer", nullable=false, options={"default"="2"})
     */
    private $vide = '2';

    /**
     * @var int
     *
     * @ORM\Column(name="blessures", type="integer", nullable=false)
     */
    private $blessures = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="honneur", type="float", precision=10, scale=0, nullable=false, options={"default"="4"})
     */
    private $honneur = '4';

    /**
     * @var float
     *
     * @ORM\Column(name="gloire", type="float", precision=10, scale=0, nullable=false, options={"default"="1"})
     */
    private $gloire = '1';

    /**
     * @var float
     *
     * @ORM\Column(name="statut", type="float", precision=10, scale=0, nullable=false, options={"default"="1"})
     */
    private $statut = '1';

    /**
     * @var int
     *
     * @ORM\Column(name="souillure", type="integer", nullable=false)
     */
    private $souillure = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="sexe", type="string", length=255, nullable=true)
     */
    private $sexe;

    /**
     * @var int|null
     *
     * @ORM\Column(name="age", type="integer", nullable=true)
     */
    private $age;

    /**
     * @var string|null
     *
     * @ORM\Column(name="taille", type="string", length=255, nullable=true)
     */
    private $taille;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cheveux", type="string", length=255, nullable=true)
     */
    private $cheveux;

    /**
     * @var string|null
     *
     * @ORM\Column(name="yeux", type="string", length=255, nullable=true)
     */
    private $yeux;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pere", type="string", length=255, nullable=true)
     */
    private $pere;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mere", type="string", length=255, nullable=true)
     */
    private $mere;

    /**
     * @var string|null
     *
     * @ORM\Column(name="freres_soeurs", type="string", length=250, nullable=true)
     */
    private $freresSoeurs;

    /**
     * @var string|null
     *
     * @ORM\Column(name="situation_familiale", type="string", length=255, nullable=true)
     */
    private $situationFamiliale;

    /**
     * @var string|null
     *
     * @ORM\Column(name="marie_a", type="string", length=255, nullable=true)
     */
    private $marieA;

    /**
     * @var string|null
     *
     * @ORM\Column(name="enfants", type="string", length=250, nullable=true)
     */
    private $enfants = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="notes", type="string", length=255, nullable=true)
     */
    private $notes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bonus_init", type="string", length=50, nullable=true, options={"default"="0G0"})
     */
    private $bonusInit = '0G0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="bonus_nd", type="integer", nullable=true)
     */
    private $bonusNd = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="bonus_recup", type="integer", nullable=true)
     */
    private $bonusRecup = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="notes_campagne", type="integer", nullable=true)
     */
    private $notesCampagne;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fleches_restantes", type="string", length=50, nullable=true)
     */
    private $flechesRestantes;

    /**
     * @var int|null
     *
     * @ORM\Column(name="koku", type="integer", nullable=true)
     */
    private $koku;

    /**
     * @var int|null
     *
     * @ORM\Column(name="bu", type="integer", nullable=true)
     */
    private $bu;

    /**
     * @var int|null
     *
     * @ORM\Column(name="zeni", type="integer", nullable=true)
     */
    private $zeni;

    /**
     * @var string|null
     *
     * @ORM\Column(name="notes_perso", type="text", length=65535, nullable=true)
     */
    private $notesPerso;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_1", type="integer", nullable=true)
     */
    private $technique1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_2", type="integer", nullable=true)
     */
    private $technique2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_3", type="integer", nullable=true)
     */
    private $technique3;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_4", type="integer", nullable=true)
     */
    private $technique4;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_5", type="integer", nullable=true)
     */
    private $technique5;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_6", type="integer", nullable=true)
     */
    private $technique6;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_7", type="integer", nullable=true)
     */
    private $technique7;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_8", type="integer", nullable=true)
     */
    private $technique8;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sorts", type="string", length=255, nullable=true)
     */
    private $sorts;

    /**
     * @var int|null
     *
     * @ORM\Column(name="terre_restants", type="integer", nullable=true)
     */
    private $terreRestants;

    /**
     * @var int|null
     *
     * @ORM\Column(name="air_restants", type="integer", nullable=true)
     */
    private $airRestants;

    /**
     * @var int|null
     *
     * @ORM\Column(name="eau_restants", type="integer", nullable=true)
     */
    private $eauRestants;

    /**
     * @var int|null
     *
     * @ORM\Column(name="feu_restants", type="integer", nullable=true)
     */
    private $feuRestants;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vide_restants", type="integer", nullable=true)
     */
    private $videRestants;

    /**
     * @var int|null
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true)
     */
    private $gameId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getClanId(): ?int
    {
        return $this->clanId;
    }

    public function setClanId(?int $clanId): self
    {
        $this->clanId = $clanId;

        return $this;
    }

    public function getClanName(): ?string
    {
        return $this->clanName;
    }

    public function setClanName(?string $clanName): self
    {
        $this->clanName = $clanName;

        return $this;
    }

    public function getFamilleId(): ?int
    {
        return $this->familleId;
    }

    public function setFamilleId(?int $familleId): self
    {
        $this->familleId = $familleId;

        return $this;
    }

    public function getFamilleName(): ?string
    {
        return $this->familleName;
    }

    public function setFamilleName(?string $familleName): self
    {
        $this->familleName = $familleName;

        return $this;
    }

    public function getEcoleId(): ?int
    {
        return $this->ecoleId;
    }

    public function setEcoleId(?int $ecoleId): self
    {
        $this->ecoleId = $ecoleId;

        return $this;
    }

    public function getEcoleName(): ?string
    {
        return $this->ecoleName;
    }

    public function setEcoleName(?string $ecoleName): self
    {
        $this->ecoleName = $ecoleName;

        return $this;
    }

    public function getArmes(): ?string
    {
        return $this->armes;
    }

    public function setArmes(?string $armes): self
    {
        $this->armes = $armes;

        return $this;
    }

    public function getEquipement(): ?string
    {
        return $this->equipement;
    }

    public function setEquipement(?string $equipement): self
    {
        $this->equipement = $equipement;

        return $this;
    }

    public function getArmure(): ?int
    {
        return $this->armure;
    }

    public function setArmure(?int $armure): self
    {
        $this->armure = $armure;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getXp(): ?int
    {
        return $this->xp;
    }

    public function setXp(?int $xp): self
    {
        $this->xp = $xp;

        return $this;
    }

    public function getXpRestant(): ?int
    {
        return $this->xpRestant;
    }

    public function setXpRestant(?int $xpRestant): self
    {
        $this->xpRestant = $xpRestant;

        return $this;
    }

    public function getJoueur(): ?string
    {
        return $this->joueur;
    }

    public function setJoueur(?string $joueur): self
    {
        $this->joueur = $joueur;

        return $this;
    }

    public function getRang(): ?int
    {
        return $this->rang;
    }

    public function setRang(?int $rang): self
    {
        $this->rang = $rang;

        return $this;
    }

    public function getReputation(): ?int
    {
        return $this->reputation;
    }

    public function setReputation(?int $reputation): self
    {
        $this->reputation = $reputation;

        return $this;
    }

    public function getCompetences(): ?string
    {
        return $this->competences;
    }

    public function setCompetences(?string $competences): self
    {
        $this->competences = $competences;

        return $this;
    }

    public function getKatas(): ?array
    {
	    return json_decode($this->katas);
    }

    public function setKatas(?array $katas): self
    {
	    if (! empty($this->katas)) {
		    $aKatas = json_decode($this->katas);
	    } else {
		    $aKatas = array();
	    }
	    array_push($aKatas, $katas);
	    $this->katas = json_encode($aKatas);

	    return $this;
    }

    public function getAvantages(): ?string
    {
        return $this->avantages;
    }

    public function setAvantages(?string $avantages): self
    {
        $this->avantages = $avantages;

        return $this;
    }

    public function getDesavantages(): ?string
    {
        return $this->desavantages;
    }

    public function setDesavantages(?string $desavantages): self
    {
        $this->desavantages = $desavantages;

        return $this;
    }

    public function getTerre(): ?int
    {
        return $this->terre;
    }

    public function setTerre(?int $terre): self
    {
        $this->terre = $terre;

        return $this;
    }

    public function getConstitution(): ?int
    {
        return $this->constitution;
    }

    public function setConstitution(?int $constitution): self
    {
        $this->constitution = $constitution;

        return $this;
    }

    public function getVolonte(): ?int
    {
        return $this->volonte;
    }

    public function setVolonte(?int $volonte): self
    {
        $this->volonte = $volonte;

        return $this;
    }

    public function getAir(): ?int
    {
        return $this->air;
    }

    public function setAir(?int $air): self
    {
        $this->air = $air;

        return $this;
    }

    public function getReflexes(): ?int
    {
        return $this->reflexes;
    }

    public function setReflexes(?int $reflexes): self
    {
        $this->reflexes = $reflexes;

        return $this;
    }

    public function getIntuition(): ?int
    {
        return $this->intuition;
    }

    public function setIntuition(?int $intuition): self
    {
        $this->intuition = $intuition;

        return $this;
    }

    public function getEau(): ?int
    {
        return $this->eau;
    }

    public function setEau(?int $eau): self
    {
        $this->eau = $eau;

        return $this;
    }

    public function getForce(): ?int
    {
        return $this->force;
    }

    public function setForce(?int $force): self
    {
        $this->force = $force;

        return $this;
    }

    public function getPerception(): ?int
    {
        return $this->perception;
    }

    public function setPerception(?int $perception): self
    {
        $this->perception = $perception;

        return $this;
    }

    public function getFeu(): ?int
    {
        return $this->feu;
    }

    public function setFeu(?int $feu): self
    {
        $this->feu = $feu;

        return $this;
    }

    public function getAgilite(): ?int
    {
        return $this->agilite;
    }

    public function setAgilite(?int $agilite): self
    {
        $this->agilite = $agilite;

        return $this;
    }

    public function getIntelligence(): ?int
    {
        return $this->intelligence;
    }

    public function setIntelligence(?int $intelligence): self
    {
        $this->intelligence = $intelligence;

        return $this;
    }

    public function getVide(): ?int
    {
        return $this->vide;
    }

    public function setVide(?int $vide): self
    {
        $this->vide = $vide;

        return $this;
    }

    public function getBlessures(): ?int
    {
        return $this->blessures;
    }

    public function setBlessures(?int $blessures): self
    {
        $this->blessures = $blessures;

        return $this;
    }

    public function getHonneur(): ?float
    {
        return $this->honneur;
    }

    public function setHonneur(?float $honneur): self
    {
        $this->honneur = $honneur;

        return $this;
    }

    public function getGloire(): ?float
    {
        return $this->gloire;
    }

    public function setGloire(?float $gloire): self
    {
        $this->gloire = $gloire;

        return $this;
    }

    public function getStatut(): ?float
    {
        return $this->statut;
    }

    public function setStatut(?float $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getSouillure(): ?int
    {
        return $this->souillure;
    }

    public function setSouillure(?int $souillure): self
    {
        $this->souillure = $souillure;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(?string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getTaille(): ?string
    {
        return $this->taille;
    }

    public function setTaille(?string $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getCheveux(): ?string
    {
        return $this->cheveux;
    }

    public function setCheveux(?string $cheveux): self
    {
        $this->cheveux = $cheveux;

        return $this;
    }

    public function getYeux(): ?string
    {
        return $this->yeux;
    }

    public function setYeux(?string $yeux): self
    {
        $this->yeux = $yeux;

        return $this;
    }

    public function getPere(): ?string
    {
        return $this->pere;
    }

    public function setPere(?string $pere): self
    {
        $this->pere = $pere;

        return $this;
    }

    public function getMere(): ?string
    {
        return $this->mere;
    }

    public function setMere(?string $mere): self
    {
        $this->mere = $mere;

        return $this;
    }

    public function getFreresSoeurs(): ?string
    {
        return $this->freresSoeurs;
    }

    public function setFreresSoeurs(?string $freresSoeurs): self
    {
        $this->freresSoeurs = $freresSoeurs;

        return $this;
    }

    public function getSituationFamiliale(): ?string
    {
        return $this->situationFamiliale;
    }

    public function setSituationFamiliale(?string $situationFamiliale): self
    {
        $this->situationFamiliale = $situationFamiliale;

        return $this;
    }

    public function getMarieA(): ?string
    {
        return $this->marieA;
    }

    public function setMarieA(?string $marieA): self
    {
        $this->marieA = $marieA;

        return $this;
    }

    public function getEnfants(): ?string
    {
        return $this->enfants;
    }

    public function setEnfants(?string $enfants): self
    {
        $this->enfants = $enfants;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getBonusInit(): ?string
    {
        return $this->bonusInit;
    }

    public function setBonusInit(?string $bonusInit): self
    {
        $this->bonusInit = $bonusInit;

        return $this;
    }

    public function getBonusNd(): ?int
    {
        return $this->bonusNd;
    }

    public function setBonusNd(?int $bonusNd): self
    {
        $this->bonusNd = $bonusNd;

        return $this;
    }

    public function getBonusRecup(): ?int
    {
        return $this->bonusRecup;
    }

    public function setBonusRecup(?int $bonusRecup): self
    {
        $this->bonusRecup = $bonusRecup;

        return $this;
    }

    public function getNotesCampagne(): ?int
    {
        return $this->notesCampagne;
    }

    public function setNotesCampagne(?int $notesCampagne): self
    {
        $this->notesCampagne = $notesCampagne;

        return $this;
    }

    public function getFlechesRestantes(): ?string
    {
        return $this->flechesRestantes;
    }

    public function setFlechesRestantes(?string $flechesRestantes): self
    {
        $this->flechesRestantes = $flechesRestantes;

        return $this;
    }

    public function getKoku(): ?int
    {
        return $this->koku;
    }

    public function setKoku(?int $koku): self
    {
        $this->koku = $koku;

        return $this;
    }

    public function getBu(): ?int
    {
        return $this->bu;
    }

    public function setBu(?int $bu): self
    {
        $this->bu = $bu;

        return $this;
    }

    public function getZeni(): ?int
    {
        return $this->zeni;
    }

    public function setZeni(?int $zeni): self
    {
        $this->zeni = $zeni;

        return $this;
    }

    public function getNotesPerso(): ?string
    {
        return $this->notesPerso;
    }

    public function setNotesPerso(?string $notesPerso): self
    {
        $this->notesPerso = $notesPerso;

        return $this;
    }

    public function getTechnique1(): ?int
    {
        return $this->technique1;
    }

    public function setTechnique1(?int $technique1): self
    {
        $this->technique1 = $technique1;

        return $this;
    }

    public function getTechnique2(): ?int
    {
        return $this->technique2;
    }

    public function setTechnique2(?int $technique2): self
    {
        $this->technique2 = $technique2;

        return $this;
    }

    public function getTechnique3(): ?int
    {
        return $this->technique3;
    }

    public function setTechnique3(?int $technique3): self
    {
        $this->technique3 = $technique3;

        return $this;
    }

    public function getTechnique4(): ?int
    {
        return $this->technique4;
    }

    public function setTechnique4(?int $technique4): self
    {
        $this->technique4 = $technique4;

        return $this;
    }

    public function getTechnique5(): ?int
    {
        return $this->technique5;
    }

    public function setTechnique5(?int $technique5): self
    {
        $this->technique5 = $technique5;

        return $this;
    }

    public function getTechnique6(): ?int
    {
        return $this->technique6;
    }

    public function setTechnique6(?int $technique6): self
    {
        $this->technique6 = $technique6;

        return $this;
    }

    public function getTechnique7(): ?int
    {
        return $this->technique7;
    }

    public function setTechnique7(?int $technique7): self
    {
        $this->technique7 = $technique7;

        return $this;
    }

    public function getTechnique8(): ?int
    {
        return $this->technique8;
    }

    public function setTechnique8(?int $technique8): self
    {
        $this->technique8 = $technique8;

        return $this;
    }

    public function getSorts(): array
    {
	    return empty($this->sorts) ? array() : json_decode($this->sorts, true);
    }

    public function setSorts(array $sorts, ?bool $doEmpty = false): self
    {
    	if ($doEmpty) {
		    $this->sorts = '';
	    } else {
		    if (!empty($this->sorts)) {
			    $aSorts = json_decode($this->sorts, true);
		    } else {
			    $aSorts = array();
		    }
		    array_push($aSorts, $sorts);
		    $this->sorts = json_encode($aSorts);
	    }
	    return $this;
    }

    public function getTerreRestants(): ?int
    {
        return $this->terreRestants;
    }

    public function setTerreRestants(?int $terreRestants): self
    {
        $this->terreRestants = $terreRestants;

        return $this;
    }

    public function getAirRestants(): ?int
    {
        return $this->airRestants;
    }

    public function setAirRestants(?int $airRestants): self
    {
        $this->airRestants = $airRestants;

        return $this;
    }

    public function getEauRestants(): ?int
    {
        return $this->eauRestants;
    }

    public function setEauRestants(?int $eauRestants): self
    {
        $this->eauRestants = $eauRestants;

        return $this;
    }

    public function getFeuRestants(): ?int
    {
        return $this->feuRestants;
    }

    public function setFeuRestants(?int $feuRestants): self
    {
        $this->feuRestants = $feuRestants;

        return $this;
    }

    public function getVideRestants(): ?int
    {
        return $this->videRestants;
    }

    public function setVideRestants(?int $videRestants): self
    {
        $this->videRestants = $videRestants;

        return $this;
    }

    public function getGameId(): ?int
    {
        return $this->gameId;
    }

    public function setGameId(?int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }


}
