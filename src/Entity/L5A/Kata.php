<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kata
 *
 * @ORM\Table(name="kata")
 * @ORM\Entity
 */
class Kata
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="anneau", type="string", length=50, nullable=false)
     */
    private $anneau;

    /**
     * @var int
     *
     * @ORM\Column(name="maitrise", type="integer", nullable=false)
     */
    private $maitrise;

    /**
     * @var string
     *
     * @ORM\Column(name="ecoles", type="string", length=50, nullable=false, options={"default"="Toutes"})
     */
    private $ecoles = 'Toutes';

    /**
     * @var string|null
     *
     * @ORM\Column(name="effet", type="text", length=65535, nullable=true)
     */
    private $effet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAnneau(): ?string
    {
        return $this->anneau;
    }

    public function setAnneau(string $anneau): self
    {
        $this->anneau = $anneau;

        return $this;
    }

    public function getMaitrise(): ?int
    {
        return $this->maitrise;
    }

    public function setMaitrise(int $maitrise): self
    {
        $this->maitrise = $maitrise;

        return $this;
    }

    public function getEcoles(): ?string
    {
        return $this->ecoles;
    }

    public function setEcoles(string $ecoles): self
    {
        $this->ecoles = $ecoles;

        return $this;
    }

    public function getEffet(): ?string
    {
        return $this->effet;
    }

    public function setEffet(?string $effet): self
    {
        $this->effet = $effet;

        return $this;
    }


}
