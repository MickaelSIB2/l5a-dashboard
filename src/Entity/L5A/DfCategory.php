<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * DfCategory
 *
 * @ORM\Table(name="df_category")
 * @ORM\Entity
 */
class DfCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="disp_position", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $dispPosition;

    /**
     * @var string|null
     *
     * @ORM\Column(name="read_authorised_roles", type="string", length=255, nullable=true)
     */
    private $readAuthorisedRoles;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDispPosition(): ?int
    {
        return $this->dispPosition;
    }

    public function setDispPosition(int $dispPosition): self
    {
        $this->dispPosition = $dispPosition;

        return $this;
    }

    public function getReadAuthorisedRoles(): ?string
    {
        return $this->readAuthorisedRoles;
    }

    public function setReadAuthorisedRoles(?string $readAuthorisedRoles): self
    {
        $this->readAuthorisedRoles = $readAuthorisedRoles;

        return $this;
    }


}
