<?php
//
///**
// * Created by PhpStorm.
// * User: Mickaël
// * Date: 03/05/2017
// * Time: 10:08
// */
//
//namespace App\Entity\L5A;
//
//use FOS\UserBundle\Model\User as BaseUser;
//use Doctrine\ORM\Mapping as ORM;
//
///**
// * @ORM\Entity
// * @ORM\Table(name="app_user")
// */
//class User extends BaseUser
//{
//	/**
//	 * @ORM\Id
//	 * @ORM\Column(type="integer")
//	 * @ORM\GeneratedValue(strategy="AUTO")
//	 */
//	protected $id;
//
//	/**
//	 * @ORM\Column(type="integer")
//	 */
//	protected $gameId;
//
//	public function __construct()
//	{
//		parent::__construct();
//		$this->roles = array('ROLE_USER');
//	}
//
//	/**
//	 * @return mixed
//	 */
//	public function getGameId() {
//		return $this->gameId;
//	}
//
//	/**
//	 * @param mixed $gameId
//	 */
//	public function setGameId($gameId) {
//		$this->gameId = $gameId;
//	}
//
//	/**
//	 * @return mixed
//	 */
//	public function getId() {
//		return $this->id;
//	}
//
//	/**
//	 * @param mixed $id
//	 */
//	public function setId($id) {
//		$this->id = $id;
//	}
//
//    /**
//     * Get enabled
//     *
//     * @return boolean
//     */
//    public function getEnabled()
//    {
//        return $this->enabled;
//    }
//    /**
//     * @var string
//     */
//    private $locale;
//
//
//    /**
//     * Set locale
//     *
//     * @param string $locale
//     *
//     * @return User
//     */
//    public function setLocale($locale)
//    {
//        $this->locale = $locale;
//
//        return $this;
//    }
//
//    /**
//     * Get locale
//     *
//     * @return string
//     */
//    public function getLocale()
//    {
//        return $this->locale;
//    }
//}
