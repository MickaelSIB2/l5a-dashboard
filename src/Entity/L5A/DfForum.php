<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * DfForum
 *
 * @ORM\Table(name="df_forum", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_6734FB05989D9B62", columns={"slug"})}, indexes={@ORM\Index(name="IDX_6734FB0512469DE2", columns={"category_id"})})
 * @ORM\Entity
 */
class DfForum
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=150, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=128, nullable=false)
     */
    private $slug;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image_url", type="string", length=255, nullable=true)
     */
    private $imageUrl;

    /**
     * @var int
     *
     * @ORM\Column(name="disp_position", type="integer", nullable=false)
     */
    private $dispPosition;

    /**
     * @var \DfCategory
     *
     * @ORM\ManyToOne(targetEntity="DfCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getDispPosition(): ?int
    {
        return $this->dispPosition;
    }

    public function setDispPosition(int $dispPosition): self
    {
        $this->dispPosition = $dispPosition;

        return $this;
    }

    public function getCategory(): ?DfCategory
    {
        return $this->category;
    }

    public function setCategory(?DfCategory $category): self
    {
        $this->category = $category;

        return $this;
    }


}
