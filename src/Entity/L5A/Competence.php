<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Competence
 *
 * @ORM\Table(name="competence")
 * @ORM\Entity
 */
class Competence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=true)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="effet", type="string", length=50, nullable=true)
     */
    private $effet;

    /**
     * @var string
     *
     * @ORM\Column(name="trait", type="string", length=50, nullable=false)
     */
    private $trait;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sous_types", type="string", length=250, nullable=true)
     */
    private $sousTypes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="specialisations", type="string", length=250, nullable=true)
     */
    private $specialisations;

    /**
     * @var string|null
     *
     * @ORM\Column(name="capacite_maitrise_1_description", type="string", length=250, nullable=true)
     */
    private $capaciteMaitrise1Description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="capacite_maitrise_1_effet", type="string", length=50, nullable=true)
     */
    private $capaciteMaitrise1Effet;

    /**
     * @var string|null
     *
     * @ORM\Column(name="capacite_maitrise_2_description", type="string", length=250, nullable=true)
     */
    private $capaciteMaitrise2Description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="capacite_maitrise_2_effet", type="string", length=50, nullable=true)
     */
    private $capaciteMaitrise2Effet;

    /**
     * @var string|null
     *
     * @ORM\Column(name="capacite_maitrise_3_description", type="string", length=250, nullable=true)
     */
    private $capaciteMaitrise3Description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="capacite_maitrise_3_effet", type="string", length=50, nullable=true)
     */
    private $capaciteMaitrise3Effet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEffet(): ?string
    {
        return $this->effet;
    }

    public function setEffet(?string $effet): self
    {
        $this->effet = $effet;

        return $this;
    }

    public function getTrait(): ?string
    {
        return $this->trait;
    }

    public function setTrait(string $trait): self
    {
        $this->trait = $trait;

        return $this;
    }

    public function getSousTypes(): ?string
    {
        return $this->sousTypes;
    }

    public function setSousTypes(?string $sousTypes): self
    {
        $this->sousTypes = $sousTypes;

        return $this;
    }

    public function getSpecialisations(): ?string
    {
        return $this->specialisations;
    }

    public function setSpecialisations(?string $specialisations): self
    {
        $this->specialisations = $specialisations;

        return $this;
    }

    public function getCapaciteMaitrise1Description(): ?string
    {
        return $this->capaciteMaitrise1Description;
    }

    public function setCapaciteMaitrise1Description(?string $capaciteMaitrise1Description): self
    {
        $this->capaciteMaitrise1Description = $capaciteMaitrise1Description;

        return $this;
    }

    public function getCapaciteMaitrise1Effet(): ?string
    {
        return $this->capaciteMaitrise1Effet;
    }

    public function setCapaciteMaitrise1Effet(?string $capaciteMaitrise1Effet): self
    {
        $this->capaciteMaitrise1Effet = $capaciteMaitrise1Effet;

        return $this;
    }

    public function getCapaciteMaitrise2Description(): ?string
    {
        return $this->capaciteMaitrise2Description;
    }

    public function setCapaciteMaitrise2Description(?string $capaciteMaitrise2Description): self
    {
        $this->capaciteMaitrise2Description = $capaciteMaitrise2Description;

        return $this;
    }

    public function getCapaciteMaitrise2Effet(): ?string
    {
        return $this->capaciteMaitrise2Effet;
    }

    public function setCapaciteMaitrise2Effet(?string $capaciteMaitrise2Effet): self
    {
        $this->capaciteMaitrise2Effet = $capaciteMaitrise2Effet;

        return $this;
    }

    public function getCapaciteMaitrise3Description(): ?string
    {
        return $this->capaciteMaitrise3Description;
    }

    public function setCapaciteMaitrise3Description(?string $capaciteMaitrise3Description): self
    {
        $this->capaciteMaitrise3Description = $capaciteMaitrise3Description;

        return $this;
    }

    public function getCapaciteMaitrise3Effet(): ?string
    {
        return $this->capaciteMaitrise3Effet;
    }

    public function setCapaciteMaitrise3Effet(?string $capaciteMaitrise3Effet): self
    {
        $this->capaciteMaitrise3Effet = $capaciteMaitrise3Effet;

        return $this;
    }


}
