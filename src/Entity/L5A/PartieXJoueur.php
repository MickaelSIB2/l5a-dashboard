<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * PartieXJoueur
 *
 * @ORM\Table(name="partie_x_joueur")
 * @ORM\Entity(repositoryClass="App\Repository\L5A\PartieXJoueurRepository")
 */
class PartieXJoueur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="game_id", type="integer", nullable=false)
     */
    private $gameId = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="joueur_id", type="integer", nullable=false)
     */
    private $joueurId = '0';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGameId(): ?int
    {
        return $this->gameId;
    }

    public function setGameId(int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }

    public function getJoueurId(): ?int
    {
        return $this->joueurId;
    }

    public function setJoueurId(int $joueurId): self
    {
        $this->joueurId = $joueurId;

        return $this;
    }


}
