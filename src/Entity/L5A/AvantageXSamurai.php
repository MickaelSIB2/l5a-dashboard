<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * AvantageXSamurai
 *
 * @ORM\Table(name="avantage_x_samurai")
 * @ORM\Entity(repositoryClass="App\Repository\L5A\AvantageXSamuraiRepository")
 */
class AvantageXSamurai
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="samurai_id", type="integer", nullable=false)
     */
    private $samuraiId;

    /**
     * @var int
     *
     * @ORM\Column(name="avantage_id", type="integer", nullable=false)
     */
    private $avantageId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="resume", type="text", length=0, nullable=true)
     */
    private $resume;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="est_cache", type="boolean", nullable=true)
     */
    private $estCache = '0';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSamuraiId(): ?int
    {
        return $this->samuraiId;
    }

    public function setSamuraiId(int $samuraiId): self
    {
        $this->samuraiId = $samuraiId;

        return $this;
    }

    public function getAvantageId(): ?int
    {
        return $this->avantageId;
    }

    public function setAvantageId(int $avantageId): self
    {
        $this->avantageId = $avantageId;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(?string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getEstCache(): ?bool
    {
        return $this->estCache;
    }

    public function setEstCache(?bool $estCache): self
    {
        $this->estCache = $estCache;

        return $this;
    }


}
