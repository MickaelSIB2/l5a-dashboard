<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="samurai_xref_spell")
 * @ORM\Entity(repositoryClass="App\Repository\L5A\SamuraiXrefSpellRepository")
 */
class SamuraiXrefSpell
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $samuraiId;

    /**
     * @ORM\Column(type="integer")
     */
    private $spellId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSamuraiId(): ?int
    {
        return $this->samuraiId;
    }

    public function setSamuraiId(int $samuraiId): self
    {
        $this->samuraiId = $samuraiId;

        return $this;
    }

    public function getSpellId(): ?int
    {
        return $this->spellId;
    }

    public function setSpellId(int $spellId): self
    {
        $this->spellId = $spellId;

        return $this;
    }
}
