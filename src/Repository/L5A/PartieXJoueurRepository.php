<?php
/**
 * Created by PhpStorm.
 * User: micka
 * Date: 10/09/2018
 * Time: 08:50
 */

namespace App\Repository\L5A;


use App\Entity\L5A\Game;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class PartieXJoueurRepository extends EntityRepository {

	/**
	 * @return array game => nbPLayers
	 */
	public function getNbJoueurs(?EntityManager $em, ?int $gameId) : array {
		$game = $em->getRepository(Game::class)->findOneBy(array('id' => $gameId));
		$qb = $this->createQueryBuilder('partieXJoueur');
		$qb->select('count(partieXJoueur.joueurId)')
		   ->where('partieXJoueur.gameId = ?1')
		   ->setParameter(1, $gameId);
		$nbJoueurs = (int)$qb->getQuery()->getResult()[0][1];
		return array(
			'entity' => $game,
			'nbJoueurs' => $nbJoueurs
		);
	}

	/**
	 * @param EntityManager $em
	 * @param $gameId
	 * @return array username, userid
	 */
	public function getJoueurs(?EntityManager $em, ?int $gameId) : array {
		$query = $em->createQuery(
			'SELECT u.id, u.username
			        FROM App\Entity\L5A\AppUser u, App\Entity\L5A\PartieXJoueur pxj
			        INDEX BY u.id
			        WHERE pxj.gameId=' . $gameId . '
			        AND pxj.joueurId = u.id');
		return $query->getResult();
	}

	public function findByJoueurId($value)
	{
		return $this->createQueryBuilder('pXj')
		            ->andWhere('pXj.joueurId = :val')
		            ->setParameter('val', $value)
		            ->getQuery()
		            ->getResult()
			;
	}
}