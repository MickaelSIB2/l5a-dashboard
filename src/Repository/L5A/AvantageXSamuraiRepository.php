<?php

namespace App\Repository\L5A;

use App\Entity\L5A\AvantageXSamurai;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AvantageXSamurai|null find($id, $lockMode = null, $lockVersion = null)
 * @method AvantageXSamurai|null findOneBy(array $criteria, array $orderBy = null)
 * @method AvantageXSamurai[]    findAll()
 * @method AvantageXSamurai[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvantageXSamuraiRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, AvantageXSamurai::class);
	}

	public function getSamuraiAdvantages(?EntityManager $em, ?int $samuraiId) : array {
		$query = $em
			->createQuery(
				'SELECT a, aXs.id, aXs.resume, aXs.estCache
FROM App\Entity\L5A\AvantageXSamurai aXs
LEFT JOIN App\Entity\L5A\Avantage a WITH a.id = aXs.avantageId
WHERE aXs.samuraiId = :samurai_id')
			->setParameter(':samurai_id', $samuraiId)
		;
		return $query->getResult();
	}

	//    /**
	//     * @return SamuraiXrefCompetences[] Returns an array of SamuraiXrefCompetences objects
	//     */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('s')
			->andWhere('s.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('s.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?SamuraiXrefCompetences
	{
		return $this->createQueryBuilder('s')
			->andWhere('s.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
