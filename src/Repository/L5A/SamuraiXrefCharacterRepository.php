<?php

namespace App\Repository\L5A;

use App\Entity\L5A\SamuraiXrefCharacter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SamuraiXrefCharacter|null find($id, $lockMode = null, $lockVersion = null)
 * @method SamuraiXrefCharacter|null findOneBy(array $criteria, array $orderBy = null)
 * @method SamuraiXrefCharacter[]    findAll()
 * @method SamuraiXrefCharacter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SamuraiXrefCharacterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SamuraiXrefCharacter::class);
    }

	public function getSamuraiCharacters(?EntityManager $em, ?int $samuraiId) : array {
		$query = $em
			->createQuery(
				'SELECT c
FROM App\Entity\L5A\SamuraiXrefCharacter sXc
LEFT JOIN App\Entity\L5A\Personnage c WITH c.id = sXc.characterId
WHERE sXc.samuraiId = :samurai_id')
			->setParameter(':samurai_id', $samuraiId)
		;
		return $query->getResult();
	}

//    /**
//     * @return NotesXPersonnage[] Returns an array of NotesXPersonnage objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NotesXPersonnage
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
