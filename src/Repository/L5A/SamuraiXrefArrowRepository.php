<?php

namespace App\Repository\L5A;

use App\Entity\L5A\SamuraiXrefArrow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SamuraiXrefArrow|null find($id, $lockMode = null, $lockVersion = null)
 * @method SamuraiXrefArrow|null findOneBy(array $criteria, array $orderBy = null)
 * @method SamuraiXrefArrow[]    findAll()
 * @method SamuraiXrefArrow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SamuraiXrefArrowRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SamuraiXrefArrow::class);
    }

	public function getArrows(?EntityManager $em, ?int $samuraiId) : array {
		$query = $em
			->createQuery(
				'SELECT a, sXa.quantity
FROM App\Entity\L5A\SamuraiXrefArrow sXa
LEFT JOIN App\Entity\L5A\Fleche a WITH a.id = sXa.arrowId
WHERE sXa.samuraiId = :samurai_id')
			->setParameter(':samurai_id', $samuraiId)
		;
		return $query->getResult();
	}
//    /**
//     * @return SamuraiXrefArrow[] Returns an array of SamuraiXrefArrow objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SamuraiXrefArrow
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
