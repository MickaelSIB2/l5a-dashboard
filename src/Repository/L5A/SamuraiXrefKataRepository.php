<?php

namespace App\Repository\L5A;

use App\Entity\L5A\SamuraiXrefKata;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SamuraiXrefKata|null find($id, $lockMode = null, $lockVersion = null)
 * @method SamuraiXrefKata|null findOneBy(array $criteria, array $orderBy = null)
 * @method SamuraiXrefKata[]    findAll()
 * @method SamuraiXrefKata[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SamuraiXrefKataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SamuraiXrefKata::class);
    }

	public function getSamuraiKatas(?EntityManager $em, ?int $samuraiId) : array {
		$query = $em
			->createQuery(
				'SELECT k
FROM App\Entity\L5A\SamuraiXrefKata sXk
LEFT JOIN App\Entity\L5A\Kata k WITH k.id = sXk.kataId
WHERE sXk.samuraiId = :samurai_id')
			->setParameter(':samurai_id', $samuraiId)
		;
		return $query->getResult();
	}

//    /**
//     * @return SamuraiXrefKata[] Returns an array of SamuraiXrefKata objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SamuraiXrefKata
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
